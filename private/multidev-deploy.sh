#!/bin/bash

# Store the mr- environment name
echo "CI_MERGE_REQUEST_IID: $CI_MERGE_REQUEST_IID"
export PANTHEON_ENV=mr-$CI_MERGE_REQUEST_IID

# Authenticate with Terminus
terminus auth:login --machine-token=$PANTHEON_MACHINE_TOKEN

echo "CI_COMMIT_SHORT_SHA: " $CI_COMMIT_SHORT_SHA

# Create a function for determining if a multidev exists
TERMINUS_DOES_MULTIDEV_EXIST()
{
    # Stash a list of Pantheon multidev environments
    PANTHEON_MULTIDEV_LIST="$(terminus multidev:list ${PANTHEON_SITE} --format=list --field=id)"

    while read -r multiDev; do
        if [[ "${multiDev}" == "$1" ]]
        then
            return 0;
        fi
    done <<< "$PANTHEON_MULTIDEV_LIST"

    return 1;
}

# If the mutltidev doesn't exist
if ! TERMINUS_DOES_MULTIDEV_EXIST $PANTHEON_ENV
then
    # Create it with Terminus
    echo "No multidev for $PANTHEON_ENV found, creating one..."
    terminus multidev:create $PANTHEON_SITE.dev $PANTHEON_ENV
else
    echo "The multidev $PANTHEON_ENV already exists, skipping creating it..."
fi

# Add the Pantheon Git repository as an additional remote
echo "adding pantheon as a remote"
git remote add pantheon $PANTHEON_GIT_URL

# Push the merge request source branch to Pantheon
echo "pushing to pantheon: git push $CI_COMMIT_SHORT_SHA:$PANTHEON_ENV --force"
git push pantheon $CI_COMMIT_SHORT_SHA:$PANTHEON_ENV --force
