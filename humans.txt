/*    Library Team      */
   
   Core Implementation Team:

     Annie Platoff (Content, training, testing)
     Rebecca Metzger (Project owner, scoping decisions)
     Ian Lessing (Developer)

  Other Team members:

   Designer: Jonathan Rissmeyer
   Events & Exhibitions: Alex Reagan
   System Administration: Scott Smith



/*    KWall Team (consultants)      */
    Alexa Haun (project manager)
    Tim Dewey (developer)
    Brett Anderson (design)


