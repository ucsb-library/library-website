## Deploying the code...

We are using the `master` branch of the git repository.

---
##### One time operation:
First step is to place a copy of the git repository on the Production Primary server: `drupalprod1`.


###### Clone the *Master* branch of the [Drupal7 git repository](https://github.library.ucsb.edu/Drupal/drupal7) to some directory on the target server

```
git clone https://github.library.ucsb.edu/Drupal/drupal7.git -b master --single-branch

```
---

### For each subsequent deploy:
Repeat the following procedure anytime you need to update code on the server.  Most commonly this will be done anytime developers make changes to the source code that are tested and ready to be put into production.

#### update the git repo on the server to have the latest code from Github
open an terminal session on the target server.


```
cd $YOUR_GIT_REPO_DIRECTORY
git pull
```

#### change file ownership
This step is needed so files moved into the webserver DocumentRoot have the correct ownership.

```
sudo chown -R nginx:nginx $YOUR_GIT_REPO_DIRECTORY/
```

#### sync code files over to the webserver DocumentRoot

```
sudo rsync --delete --stats --recursive --checksum --cvs-exclude --verbose --exclude="sites/default/files/" --exclude="sites/default/files2016-11-09/" --exclude="settings.php" --exclude="phpini.php" --exclude="tmp/" $YOUR_GIT_REPO_DIRECTORY/ /var/www/html/drupal
```

#### change file ownership back
You may need to change the username and/or group in the `chown` operation below

```
sudo chown -R drupalsync:drupalsync $YOUR_GIT_REPO_DIRECTORY/
```

#### clear drupal caches

```
drush cc all
```

#### Notify interested parties that code has been deployed.

If you updated Drupal core be sure to update [Drupal Core Update History](https://wiki.library.ucsb.edu/display/SWS/Drupal+Core+Update+History) in the Wiki.

