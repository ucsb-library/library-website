$(document).ready(
  function (){
    console.log('hello');
  }
);
$(window).load(function() {
  $('#flexslider-1.flexslider').flexslider({
        animation: 'fade',
        animationLoop: true,
        controlNav: true
  });
  $('#flexslider-2.flexslider').flexslider({
        animation: 'fade',
        animationLoop: true,
        controlNav: true
  });
  $('#flexslider-3.flexslider').flexslider({
        animation: 'fade',
        animationLoop: true,
        controlNav: true
  });
  var $object = $('.slick-carosel-example');
  if($object.length) {
    // there's at least one matching element
    $('.slick-carosel-example').slick({
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 3,
  //variableWidth: true,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
      }
    },
    {
      breakpoint: 967,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

  }

});