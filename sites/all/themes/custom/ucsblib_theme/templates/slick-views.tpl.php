<?php
/**
 * @file
 * Default theme implementation for the Slick views template.
 *
 * Available variables:
 * - $rows: The array of items.
 * - $options: Array of available settings via Views UI.
 */
?>
<?php print $wrapper_prefix; ?>
  <?php foreach ($rows as $id => $row): ?>
    <div class="issue324 slick--slider slick--float slick--ondemand"><?php print render($row); ?></div>
  <?php endforeach; ?>
<?php print $wrapper_suffix; ?>
