<?php

/**
 * @file views-limit-grouping.tpl.php
 * Basically, just a copy of views-view-unformatted.tpl.php.
 */
?>
<div class="views-limit-grouping-group">
  <?php if($title == 'Page') { ?>
  <div class="item-list"><ul>
  <?php foreach ($rows as $id => $row){ if($id < 2) { ?>
    <li class="views-row views-row-<?php print $zebra; ?> <?php print $classes; ?>">
      <?php print $row; ?>
    </li>
    <?php } else { ?>
      <br /> <button><a href="search/pages<?php if(isset($_GET['content'])): ?>?content=<?php print $_GET['content']; endif;?>">See More Results</a></button>
  <?php break; } } } else {
  ?>
  </ul></div>

  <?php if (!empty($title)): ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
  <div class="item-list"><ul>
  <?php foreach ($rows as $id => $row): ?>
    <li class="views-row views-row-<?php print $zebra; ?> <?php print $classes; ?>">
      <?php print $row; ?>
    </li>
  <?php endforeach; } ?>
  </ul></div>
</div>
