// Remove default value in table dialog for width (which is 500 by default).

CKEDITOR.plugins.add('kwall_tablewidth', {
});

CKEDITOR.on('dialogDefinition', function(ev) {
  // Take the dialog name and its definition from the event data.
  var dialogName = ev.data.name;
  var dialogDefinition = ev.data.definition;

  // Check if the definition is from the dialog we're
  // interested on (the "Table" dialog).
  if (dialogName == 'table') {
    // Get a reference to the "Table Info" tab.
    var info = dialogDefinition.getContents('info');

    // Set default width to 100%.
    info.get('txtWidth')['default'] = '100%';
  }
});
