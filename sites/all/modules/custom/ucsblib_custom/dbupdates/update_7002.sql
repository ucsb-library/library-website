-- removed module cleanup
-- fixup data due to missing modules:
-- apachesolr
DELETE FROM system WHERE type = 'module' AND name = 'apachesolr';
-- search_api_solr
DELETE FROM system WHERE type = 'module' AND name = 'search_api_solr';
-- apachesolr_views
DELETE FROM system WHERE type = 'module' AND name = 'apachesolr_views';
-- apachesolr_search
DELETE FROM system WHERE type = 'module' AND name = 'apachesolr_search';
-- apachesolr_access
DELETE FROM system WHERE type = 'module' AND name = 'apachesolr_access';
-- delete all submissions from remote_addr = 58.49.150.170  these were all on Feb 7
DELETE FROM webform_submitted_data WHERE sid IN (select sid FROM webform_submissions WHERE remote_addr = '58.49.150.170');
DELETE FROM webform_submissions WHERE remote_addr = '58.49.150.170';
-- delete submissions from IP  addresses that are known abusers
DELETE FROM webform_submitted_data WHERE sid IN (select sid FROM webform_submissions WHERE remote_addr like  '46.161.9%');
DELETE FROM webform_submissions WHERE remote_addr like  '46.161.9%';
-- delete all submission where user used rambler.ru address
-- this time we're deleting based on data values rather than remote IP
DELETE FROM webform_submissions WHERE sid IN (select sid from webform_submitted_data where data like  '%rambler.ru%');
DELETE FROM webform_submitted_data WHERE sid IN (select sid FROM webform_submitted_data WHERE data like '%rambler.ru%');
-- delete all submission before 2016-12-21 which is when we went live with D7.  All these were tests or old USC submissions.
-- first real submission was 12977
DELETE FROM webform_submissions WHERE sid < 12977;  -- 2633 rows
DELETE FROM webform_submitted_data WHERE sid < 12977;  -- 15018 rows
-- delete submissions for missing nodes (usc forms)
-- node 13716, 14141, 19166, 11961, 11566, 10756, 8702
DELETE FROM webform_submitted_data WHERE nid IN (13716, 14141, 19166, 11961, 11566, 10756, 8702);
DELETE FROM webform_submissions WHERE nid IN (13716, 14141, 19166, 11961, 11566, 10756, 8702);
-- delete data for nodes that don't exist
DELETE FROM webform where nid IN (select w.nid from webform w left join node n using(nid) Where n.nid IS NULL);
DELETE FROM webform_roles where nid IN (select w.nid from webform_roles w left join node n using(nid) Where n.nid IS NULL);
DELETE FROM webform_emails where nid IN (select w.nid from webform_emails w left join node n using(nid) Where n.nid IS NULL);
DELETE FROM webform_component where nid IN (select w.nid from webform_component w left join node n using(nid) Where n.nid IS NULL);
DELETE FROM webform_conditional where nid IN (select w.nid from webform_conditional w left join node n using(nid) Where n.nid IS NULL);
DELETE FROM webform_conditional_rules where nid IN (select w.nid from webform_conditional_rules w left join node n using(nid) Where n.nid IS NULL);
DELETE FROM webform_conditional_actions where nid IN (select w.nid from webform_conditional_actions w left join node n using(nid) Where n.nid IS NULL);
DELETE FROM webform_last_download where nid IN (select w.nid from webform_last_download w left join node n using(nid) Where n.nid IS NULL);
-- delete webform data rows for nodes which are not of content type webform
DELETE FROM webform where nid = 11196;
DELETE FROM webform_component where nid = 11196;
DELETE FROM webform_conditional where nid = 11196;
DELETE FROM webform_conditional_actions where nid = 11196;
DELETE FROM webform_conditional_rules where nid = 11196;
DELETE FROM webform_roles where nid = 11196;
-- delete submissions which mention certain drugs
DELETE FROM webform_submissions WHERE sid IN (select sid from webform_submitted_data where data like  '%cialis%'); -- 168 rows
DELETE FROM webform_submitted_data WHERE sid IN (select sid FROM webform_submitted_data WHERE data like '%cialis%'); -- 1987 rows
DELETE FROM webform_submissions WHERE sid IN (select sid from webform_submitted_data where data like  '%levitra%'); -- levitra 46 rows
DELETE FROM webform_submitted_data WHERE sid IN (select sid FROM webform_submitted_data WHERE data like '%levitra%');
DELETE FROM webform_submissions WHERE sid IN (select sid from webform_submitted_data where data like  '%viagra%');
DELETE FROM webform_submitted_data WHERE sid IN (select sid FROM webform_submitted_data WHERE data like '%viagra%');
DELETE FROM webform_submissions WHERE sid IN (select sid from webform_submitted_data where data like  '%propecia%' OR data like '%rogaine%') ;
DELETE FROM webform_submitted_data WHERE sid IN (select sid FROM webform_submitted_data WHERE data like '%propecia%' OR data like '%rogaine%');
-- delete submissions which mention common abuse/spam terms
DELETE FROM webform_submissions    WHERE sid IN (select sid from webform_submitted_data where data like '%bit.ly%');
DELETE FROM webform_submitted_data WHERE sid IN (select sid FROM webform_submitted_data WHERE data like '%bit.ly%');  -- 618
DELETE FROM webform_submissions WHERE sid in (select sid from webform_submitted_data where data = 'dtltluod' ) ;
DELETE FROM webform_submitted_data where data = 'dtltluod'  ; 
-- delete submission from known abuser email addresses
DELETE FROM webform_submissions WHERE sid IN (select sid from webform_submitted_data where data like  '%yuguhun88@hotmail.com%');
DELETE FROM webform_submitted_data WHERE sid IN (select sid FROM webform_submitted_data WHERE data like '%yuguhun88@hotmail.com%');
DELETE FROM webform_submissions WHERE sid IN (select sid from webform_submitted_data where data like  '%jfvynms4281rt@hotmail.com%');
DELETE FROM webform_submitted_data WHERE sid IN (select sid FROM webform_submitted_data WHERE data like '%jfvynms4281rt@hotmail.com%');
DELETE FROM webform_submissions WHERE sid IN (select sid from webform_submitted_data where data like  '%ziuwpm@xfyvrm.com%');
DELETE FROM webform_submitted_data WHERE sid IN (select sid FROM webform_submitted_data WHERE data like '%ziuwpm@xfyvrm.com%');
-- delete test submissions
DELETE FROM webform_submissions WHERE sid IN (select sid from webform_submitted_data where data like  '%test@ucsb.edu%');
DELETE FROM webform_submitted_data WHERE sid IN (select sid FROM webform_submitted_data WHERE data like '%test@ucsb.edu%');
-- delete submissions from very similar email addresses
DELETE ws FROM webform_submissions ws LEFT JOIN webform_submitted_data wsd ON ws.sid = wsd.sid WHERE ws.nid = 2073 AND wsd.cid = 12 AND SOUNDEX(SUBSTRING_INDEX(wsd.data, '@', 1)) = 'I512';
-- delete purchase suggestons submissions from .ru emails with a URL in the comments/notes field
-- I couldn't find a single legitimate submission from this combo of attributes.  if their email was *.ru and they had a URL in the notes field it was always a bogus phishing or scam URL
DELETE webform_submissions from webform_submissions where sid in(select sid FROM webform_submitted_data wsd WHERE nid = 2073 AND cid = 17 AND sid in (select sid from webform_submitted_data where nid = 2073 and cid = 12 AND data like '%.ru') AND data like '%http%'); -- 2225 rows
DELETE from webform_submitted_data WHERE nid = 2073 AND sid in (select sid from webform_submitted_data where nid = 2073 and cid = 12 AND data like '%.ru') AND data like '%http%'; -- 3409 rows
-- webform_submissions without data
DELETE ws FROM webform_submissions ws LEFT JOIN webform_submitted_data wsd ON ws.sid = wsd.sid WHERE wsd.sid IS NULL; -- 414 rows
-- webform_submitted_data without a corresponding row in webform_submissions
DELETE wsd FROM webform_submitted_data wsd LEFT JOIN  webform_submissions ws ON wsd.sid = ws.sid WHERE ws.sid IS NULL; -- 388 rows
-- .xyz is common abuser domain 
START TRANSACTION;
DELETE FROM webform_submissions WHERE sid IN (SELECT sid FROM webform_submitted_data WHERE nid = 2073 AND data like '%.xyz%') ;
DELETE FROM webform_submitted_data WHERE nid = 2073 AND data like '%.xyz%';
COMMIT;
-- delete responses that mention .pl domain as abusers use it
START TRANSACTION;
DELETE FROM webform_submissions WHERE sid IN (SELECT sid FROM webform_submitted_data WHERE nid = 2073 AND data like '%.pl%') ;
DELETE FROM webform_submitted_data WHERE nid = 2073 AND data like '%.pl%';
COMMIT;
-- delete responses that mention .ovout.com domain as abusers use it
START TRANSACTION;
DELETE FROM webform_submissions WHERE sid IN (SELECT sid FROM webform_submitted_data WHERE nid = 2073 AND data like '%.ovout.com%') ;
DELETE FROM webform_submitted_data WHERE nid = 2073 AND data like '%.ovout.com%';
COMMIT;
-- delete responses that mention .ru domain as abusers use it
START TRANSACTION;
DELETE FROM webform_submissions WHERE sid IN (SELECT sid FROM webform_submitted_data WHERE nid = 2073 AND data like '%.ru%') ;
DELETE FROM webform_submitted_data WHERE nid = 2073 AND data like '%.ru%';
COMMIT;
-- delete responses that mention lix in certain fields as abusers use it
START TRANSACTION;
DELETE FROM webform_submissions WHERE sid IN (SELECT sid FROM webform_submitted_data WHERE nid = 2073 AND cid IN(1, 3,4,5,12,13,14) AND data like '%lix%') ;
DELETE FROM webform_submitted_data WHERE nid = 2073 AND cid IN(1, 3,4,5,12,13,14) AND data like '%lix%';
COMMIT;
-- delete responses that mention porn as abusers are often promoting such sites
START TRANSACTION;
DELETE FROM webform_submissions WHERE sid IN (SELECT sid FROM webform_submitted_data WHERE nid = 2073 AND data like '%porn%') ;
DELETE FROM webform_submitted_data WHERE nid = 2073 AND data like '%porn%';
COMMIT;
-- delete responses that mention bitcoin as abusers are often promoting such sites
START TRANSACTION;
DELETE FROM webform_submissions WHERE sid IN (SELECT sid FROM webform_submitted_data WHERE nid = 2073 AND data like '%bitcoin%') ;
DELETE FROM webform_submitted_data WHERE nid = 2073 AND data like '%bitcoin%';
COMMIT;
-- delete responses that mention cannabis as abusers are often promoting such sites
START TRANSACTION;
DELETE FROM webform_submissions WHERE sid IN (SELECT sid FROM webform_submitted_data WHERE nid = 2073 AND data like '%cannabis%') ;
DELETE FROM webform_submitted_data WHERE nid = 2073 AND data like '%cannabis%';
COMMIT;
-- delete responses that mention cbd oil as abusers are often promoting such sites
START TRANSACTION;
DELETE FROM webform_submissions WHERE sid IN (SELECT sid FROM webform_submitted_data WHERE nid = 2073 AND data like '%cbd oil%') ;
DELETE FROM webform_submitted_data WHERE nid = 2073 AND data like '%cbd oil%';
COMMIT;
