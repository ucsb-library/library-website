(function ($, Drupal, window, document, undefined) {
  Drupal.behaviors.ucsblbi_custom = {
    attach: function(context, settings) {
      $('table').addClass('tablesaw').attr('data-tablesaw-mode', 'swipe').attr('data-tablesaw-minimap', '');
      if (!$('.accessible-mega-menu').hasClass('noclick-processed')) {
	      $('.accessible-mega-menu').addClass('noclick-processed').find('div.menu-level-1 > ul > li.menu__item > a').click(function(e) {
		      e.preventDefault();
	      });
      }
      if (!$('.kwall-slide-in-nav-menu').hasClass('noclick-processed')) {
	      $('.kwall-slide-in-nav-menu').addClass('noclick-processed').find('ul.accordion-nav-top-level-menu > li.menu__item > a.menu__link').click(function(e) {
		      e.preventDefault();
	      });
      }
    }
  };
})(jQuery, Drupal, this, this.document);
