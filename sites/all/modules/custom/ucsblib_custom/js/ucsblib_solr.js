(function ($, Drupal, window, document, undefined) {
  Drupal.behaviors.solr_search = {
    attach: function(context, settings) {
      $('body').once('solr_search', function() {
	      $('a.solr-header-link').click(function(e){
		      e.preventDefault();
		      $(this).fadeOut(250, function() {
					  $('.solr-search-header-form').show("slide", { direction: "right" }, 250);
            $('.solr-search-header-form #edit-content').focus();		  
			  	});
	      });
	      $('a.close-search').click(function(e){
		      e.preventDefault();
		      $('.solr-search-header-form').hide("slide", { direction: "right" }, 350, function() {
					  $('a.solr-header-link').fadeIn(250);
			  	});
	      });
	    });
    }
  };
})(jQuery, Drupal, this, this.document);
