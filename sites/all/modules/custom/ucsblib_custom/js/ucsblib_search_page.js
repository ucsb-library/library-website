(function ($) {
  Drupal.behaviors.search_page = {
    attach: function (context, settings) {
      $('body').once('search_page', function() {
				var keyword = Drupal.settings.ucsblib_custom.keyword;
				var main_search = Drupal.settings.ucsblib_custom.main_search;
				var research_database_page = Drupal.settings.ucsblib_custom.research_database_page;
				var news_events_exhibitions_page = Drupal.settings.ucsblib_custom.news_events_exhibitions_page;
				var buttoncount = 1;
				
				$('div.view-content .item-list ul').each(function(){
					if ($(this).children('li').length > 3) {
						$(this).parent().append('<button class="search-show-more search-show-more-'+buttoncount+'">see more results</button>').children('.search-show-more').toggle(function(){
							$(this).parent().find('li:gt(2)').slideDown();
							console.log('conut = ' + buttoncount + ' & main_search = ' + main_search);
							if ($(this).hasClass('search-show-more-1') && main_search === true) {
								$(this).text('go to next page >');
							} else if ($(this).hasClass('search-show-more-1') && main_search === false) {
								$(this).remove();
							}
							if ($(this).hasClass('search-show-more-2') && research_database_page === true) {
								$(this).text('go to next page >');
							} else if ($(this).hasClass('search-show-more-2') && research_database_page === false) {
								$(this).remove();
							}
							if ($(this).hasClass('search-show-more-3') && news_events_exhibitions_page === true) {
								$(this).text('go to next page >');
							} else if ($(this).hasClass('search-show-more-3') && news_events_exhibitions_page === false) {
								$(this).remove();
							}
						}, function() {
							if ($(this).hasClass('search-show-more-1') && main_search === true) {
								window.location = '/search/results?content='+keyword+'&page=1';
							}
							if ($(this).hasClass('search-show-more-2') && research_database_page === true) {
								window.location = '/search/research-databases?content='+keyword+'&page=1';
							}
							if ($(this).hasClass('search-show-more-3') && news_events_exhibitions_page === true) {
								window.location = '/search/news-events-exhibits?content='+keyword+'&page=1';
							}
						});
						$(this).children('li:gt(2)').hide();
						buttoncount++;
					}
				});
    	});
    }
  };
})(jQuery);
