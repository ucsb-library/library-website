(function ($) {
  Drupal.behaviors.ucsblib_select2 = {
    attach: function(context, settings) {
      $('.views-exposed-form select', context).select2({
			  minimumResultsForSearch: Infinity
			});
      $('.node-type-webform select', context).select2({
			  minimumResultsForSearch: Infinity
			});
      $('.library-search-forms select', context).select2({
			  minimumResultsForSearch: Infinity
			}).hide();
			$('.hp-search-controls-mobile select', context).select2({
			  minimumResultsForSearch: Infinity
			});
    }
  };
})(jQuery);