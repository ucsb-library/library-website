(function ($, Drupal, window, document, undefined) {
  Drupal.behaviors.uscblib_staff_directory = {
    attach: function(context, settings) {
		  $('body').once('uscblib_staff_directory', function() {
		    path = window.location.href.toString().split(window.location.host)[1];
			  if (path.substr(0, 1) === '/') {
			    path = path.substr(1);
			  }
			  
			  //get query string and path
			  querystring = path.split('?');
			  path = querystring[0].split('/');
			  if (querystring[1] != undefined) {
			  	querystring = "?" + querystring[1];
			  } else {
				  querystring = "";
			  }
			  
			  //glossary filter value
			  active_glossary = "";
			  if (Drupal.settings.ucsblib_custom != undefined) {
				  active_glossary = "/" + Drupal.settings.ucsblib_custom.active_glossary;
			  }
			  
			  //menu and glossary links
			  $people = $('#staff-directory-link > a');
			  $administration = $('#admin-directory-link > a');
			  $az = $('div.attachment.attachment-before div.views-summary.views-summary-unformatted > a');
			  
			  //check to see if the links should be active
			  checkPath(path, $people);
			  checkPath(path, $administration);
			  
			  //if view mode is in list format (table)
			  if (path[1] == 'list' || path[2] == 'list') {
				  $people.attr('href', $people.attr('href') + "/list");
					$administration.attr('href', $administration.attr('href') + "/list");
				  
				  //loop through glossary links and append list to keep view mode
				  //and append querystring to keep filter settings
					$az.each(function(){
				    $az_path = $(this).attr('href');
				    if ($az_path.substr(0, 1) === '/') {
					    $az_path = $az_path.substr(1);
					  }
					  $az_path = $az_path.split('/');
					  if ($(this).text() == "ALL") {
						  $(this).attr('href', $(this).attr('href') + "/list" + querystring);
					  } else if (path[1] == 'list') {
				    	$(this).attr('href', '/' + $az_path[0] + '/list/' + $az_path[1] + querystring);
				    } else {
					    $(this).attr('href', '/' + $az_path[0] + '/' + $az_path[1] + '/list/' + $az_path[2] + querystring);
				    }
			    });
				} else {
					$az.each(function(){
						//it's not list view so just append querystring
						$(this).attr('href', $(this).attr('href') + querystring);
			    });
				}
				
				//append glossary filter and querystring to view mode menu
				$('#table-view-switch > a').attr('href', $('#table-view-switch > a').attr('href') + active_glossary + querystring);
				$('#grid-view-switch > a').attr('href', $('#grid-view-switch > a').attr('href') + active_glossary + querystring);
			
				$('#block-menu-menu-staff-directory-menu ul.menu li a.active').click(function(e){
					if ($(window).width() < 768) {
						e.preventDefault();
						var hiddenLinks = $('#block-menu-menu-staff-directory-menu ul.menu li').not('.active-trail');
						if ($(this).hasClass('open')) {
							view_footer_height = $(this).outerHeight();
							hiddenLinks.fadeOut( 200, function() {
						    $('.staff-directory-view .az-staff-directory .view-footer').stop(true, true).animate({height:view_footer_height},200);
						  });
							$(this).removeClass('open');
						} else {
							view_footer_height = $(this).outerHeight() * $('#block-menu-menu-staff-directory-menu ul.menu li').length;
							$('.staff-directory-view .az-staff-directory .view-footer').stop(true, true).animate({height:view_footer_height},200);
							hiddenLinks.fadeIn();
							$(this).addClass('open');
						}
					}
				});
				$('.staff-directory-view .staff-listing .staff-listing-name').click(function(){
					if ($(window).width() < 768) {
						if ($(this).hasClass('open')) {
							$(this).parent().find('.staff-directory-email, .staff-directory-phone').fadeOut(100);
							$(this).removeClass('open');
						} else {
							$(this).parent().find('.staff-directory-email, .staff-directory-phone').fadeIn(100);
							$(this).addClass('open');
						}
					}
				});
				$('.staff-directory-view .staff-table-listing .staff-listing-name').click(function(){
					if ($(window).width() < 768) {
						if ($(this).hasClass('open')) {
							$(this).parent().find('.staff-directory-email, .staff-directory-phone').fadeOut(100);
							$(this).removeClass('open');
						} else {
							$(this).parent().find('.staff-directory-email, .staff-directory-phone').css({'display':'block'});
							$(this).addClass('open');
						}
					}
				});
				$('.staff-directory-view .staff-listing .staff-listing-name a').click(function(e){
					if ($(window).width() < 768) {
						e.preventDefault();
					}
				});
				$('.staff-directory-view .staff-table-listing .staff-listing-name a').click(function(e){
					if ($(window).width() < 768) {
						e.preventDefault();
					}
				});
			});
		}
  };
  
  //checks to see if the links should be marked active
  function checkPath(path, $link) {
	  if (path[1] == 'list' || path[2] == 'list') {
		  if (path[1] == 'list' && $link.attr('href') == ("/" + path[0])) {
	    	$link.addClass('active').parent().addClass('active-trail');
	    } else if (path[2] == 'list' && $link.attr('href') == ("/" + path[0] + "/" + path[1])) {
		    $link.addClass('active').parent().addClass('active-trail');
	    }
	  }
  }
  
})(jQuery, Drupal, this, this.document);
