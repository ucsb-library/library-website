(function ($, Drupal, window, document, undefined) {
  Drupal.behaviors.uscblib_staff_directory = {
    attach: function(context, settings) {
		  $('body').once('uscblib_subject_librarians', function() {
				$('#block-menu-menu-staff-directory-menu ul.menu li a.active').click(function(e){
					if ($(window).width() < 768) {
						e.preventDefault();
						var hiddenLinks = $('#block-menu-menu-staff-directory-menu ul.menu li').not('.active-trail');
						if ($(this).hasClass('open')) {
							hiddenLinks.fadeOut(200);
							$(this).removeClass('open');
						} else {
							hiddenLinks.fadeIn(200);
							$(this).addClass('open');
						}
					}
				});
				$('.view-subject-librarians .subject-librarian-name').click(function(e){
					if ($(window).width() <= 767) {
						if ($(this).hasClass('open')) {
							$(this).parent().find('.subject-librarian-email, .subject-librarian-phone').fadeOut(100);
							$(this).removeClass('open');
						} else {
							$(this).parent().find('.subject-librarian-email, .subject-librarian-phone').css({'display':'block'});
							$(this).addClass('open');
						}
					}
				});
				$('.view-subject-librarians .subject-librarian-name a').click(function(e){
					if ($(window).width() <= 767) {
						e.preventDefault();
					}
				});
			});
		}
	}
})(jQuery, Drupal, this, this.document);
