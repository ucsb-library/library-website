<?php
/**
 * @file
 * Module file for ucsblib_library_search. 
 */

/**
 * Implements hook_block_info().
 *
 */

function ucsblib_library_search_block_info() {
  $blocks['oac_sidebar_block'] = array(
    'info' => t('Online archive of California sidebar search block'),
    'cache' => DRUPAL_CACHE_PER_ROLE,
  );
  $blocks['homepage_search_block'] = array(
    'info' => t('Home Page search block'),
    'cache' => DRUPAL_CACHE_PER_ROLE,
  );
  $blocks['research_page_search_forms'] = array(
    'info' => t('Research Page search block'),
    'cache' => DRUPAL_CACHE_PER_ROLE,
  );
  $blocks['custom_login_block'] = array(
    'info' => t('Custom offsite login block'),
    'cache' => DRUPAL_CACHE_PER_ROLE,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 *
 */

function ucsblib_library_search_block_view($delta = '') {
  switch ($delta) {
    case 'oac_sidebar_block':
      $block['subject'] = t('Online archive of California');
      $block['content'] = drupal_get_form('oac_search_form');
    break;
	case 'homepage_search_block':
    $hp_search_controls = '
    	<div class="hp-search-controls-mobile">
  			<div class="hp-mobile-select">
  				<select name="hp-mobile-list" class="use-select-2 form-select">
  					<option value="hp-search-library-catalog">UC Library Search</option>
            <option value="hp-search-melvyl">Course Reserves</option>
  					<option value="hp-research-databases">Articles & Databases</option>
            <option value="hp-search-research-guides">Research Guides</option>
  					<option value="hp-search-alexandria">Alexandria Digital Research Library</option>
  					<option value="hp-search-oac">Archives & Manuscripts</option>
  				</select>
  			</div>
  		</div>
  		<div class="hp-search-controls-wrapper">
			<div class="overlay"><a href="#" data-target="hp-search-library-catalog" class="hp-search-links-active"><span>UC Library Search</span></a></div>
	    	<ul class="hp-search-controls">
	    	  <li><a href="#" data-target="hp-search-library-catalog" class="hp-search-links-active"><span>UC Library Search</span></a></li>
          <li><a href="#" data-target="hp-search-melvyl" data-line="63"><span>Course Reserves</span></a></li>
	    	  <li><a href="https://guides.library.ucsb.edu/az/databases" id="springshare-db-list" target="_self"><span>Articles & Databases</span></a></li>

          <li><a href="#" data-target="hp-search-research-guides"><span>Research Guides</span></a></li>
	    	  <li><a href="#" data-target="hp-search-alexandria"><span>Alexandria Digital Research Library</span></a></li>
	    	  <li><a href="#" data-target="hp-search-oac"><span>Archives & Manuscripts</span></a></li>
	    	</ul>
	    </div>';

    $library_catalog_form = drupal_get_form('library_catalog_search_form');
    $alexandria_form = drupal_get_form('alexandria_digital_research_library_search_form');
    $melvyl_catalog = drupal_get_form('melvyl_catalog_search_form');
    $research_guides = drupal_get_form('research_guides_search_form');
    $research_databases_subject = drupal_get_form('research_databases_subject_search_form');
    $oac = drupal_get_form('oac_homepage_search_form');

    $block['content'] = array(
	    'hp_search_controls' => array('#markup' => $hp_search_controls),
	    'form_wrap_open' => array('#markup' => '<div class="library-search-forms">'),
			'library_catalog_form' => array(
			  '#markup' => render($library_catalog_form),
			),
			'alexandria_form' => array(
			  '#markup' => render($alexandria_form),
			),
			'research_databases' => array(
			  '#markup' => render($research_databases_subject),
			),
			'melvyl_catalog' => array(
			  '#markup' => render($melvyl_catalog),
			),
			'research_guides' => array(
			  '#markup' => render($research_guides),
			),
			'oac' => array(
			  '#markup' => render($oac),
			),
			'form_wrap_close' => array('#markup' => '</div>'),
			'#attached' => array
        (
          'js' => array(
          	drupal_get_path("module", "ucsblib_library_search").'/js/jquery.matchHeight-min.js',
						drupal_get_path("module", "ucsblib_library_search").'/js/ucsblib_library_search.js'
					)
        ),
	  );
    break;

    case 'research_page_search_forms':
    	$hp_search_controls = '
    		<div class="hp-search-controls-mobile">
    			<p class="hp-mobile-select-title">Start Your Search</p>
    			<div class="hp-mobile-select">
    				<select name="hp-mobile-list" class="use-select-2 form-select">
						<option value="hp-search-library-catalog">UC Library Search</option>
              <option value="hp-search-melvyl">Course Reserves</option>
	  					<option value="hp-research-databases">Articles & Databases</option>
              <option value="hp-search-research-guides">Research Guides</option>
	  					<option value="hp-search-alexandria">Alexandria Digital Research Library</option>
	  					<option value="hp-search-oac">Archives & Manuscripts</option>
    				</select>
    			</div>
    		</div>
	    	<div class="hp-search-controls-wrapper">
	  			<div class="overlay"><a href="#" data-target="hp-search-library-catalog" class="hp-search-links-active"><span>UC Library Search</span></a></div>
		    	<ul class="hp-search-controls">
		    	  <li><a href="#" data-target="hp-search-library-catalog" class="hp-search-links-active"><span>UC Library Search</span></a></li>
            <li><a href="#" data-target="hp-search-melvyl"><span data-line="129">Course Reserves</span></a></li>
		    	  <li><a href="https://guides.library.ucsb.edu/az/databases" data-target="hp-research-databases" id="springshare-db-list"><span>Articles & Databases</span></a></li>
            <li><a href="#" data-target="hp-search-research-guides"><span>Research Guides</span></a></li>
		    	  <li><a href="#" data-target="hp-search-alexandria"><span>Alexandria Digital Research Library</span></a></li>
		    	  <li><a href="#" data-target="hp-search-oac"><span>Archives & Manuscripts</span></a></li>
		    	</ul>
		    </div>';

	    $library_catalog_form = drupal_get_form('library_catalog_search_form');
	    $alexandria_form = drupal_get_form('alexandria_digital_research_library_search_form');
	    $melvyl_catalog = drupal_get_form('melvyl_catalog_search_form');
	    $research_guides = drupal_get_form('research_guides_search_form');
	    $research_databases_subject = drupal_get_form('research_databases_subject_search_form');
	    $oac = drupal_get_form('oac_homepage_search_form');

	    $block['content'] = array(
		    'hp_search_controls' => array('#markup' => $hp_search_controls),
		    'form_wrap_open' => array('#markup' => '<div class="library-search-forms">'),
				'library_catalog_form' => array(
				  '#markup' => render($library_catalog_form),
				),
				'alexandria_form' => array(
				  '#markup' => render($alexandria_form),
				),
				'research_guides' => array(
				  '#markup' => render($research_guides),
				),
				'melvyl_catalog' => array(
				  '#markup' => render($melvyl_catalog),
				),
				'research_databases' => array(
				  '#markup' => render($research_databases_subject),
				),
				'oac' => array(
				  '#markup' => render($oac),
				),
				'form_wrap_close' => array('#markup' => '</div>'),
				'#attached' => array
	        (
	          'js' => array(
	          	drupal_get_path("module", "ucsblib_library_search").'/js/jquery.matchHeight-min.js',
							drupal_get_path("module", "ucsblib_library_search").'/js/ucsblib_library_search.js'
						)
	        ),
				);
    break;

    case 'custom_login_block':
      $my_ill = drupal_get_form('my_ill_login_form');

      $custom_login_controls = '
      	<ul class="custom-login-controls">
      	  <li><a href="https://proxy.library.ucsb.edu/login" data-target="custom-login-off-campus"><i class="fa fa-chevron-right" aria-hidden="true"></i> Off-Campus Login</a></li>
      	  <li><a href="https://search.library.ucsb.edu/discovery/account?vid=01UCSB_INST:UCSB"><i class="fa fa-chevron-right" aria-hidden="true"></i> My Library Account</a></li>
      	  <li><a href="#" data-target="custom-login-my-ill" class="custom-login-form custom-login-links-active"><i class="fa fa-chevron-right" aria-hidden="true"></i> My ILL Requests</a></li>
      	  <li><a href="https://researchspecial.library.ucsb.edu/logon/"><i class="fa fa-chevron-right" aria-hidden="true"></i> My Special Collections Research Account</a></li>
      	  <li><a href="/node/1352"><i class="fa fa-chevron-right" aria-hidden="true"></i> Help</a></li>
      	</ul>';
      $block['content'] = array(
	    	'custom_login_controls' => array('#markup' => $custom_login_controls),
				'form_wrapper_open' => array('#markup' => '<div class="custom-login-forms">'),
				'off_campus_login_form' => array(
					'#markup' => render($off_campus),
				),
				'my_ill_login_form' => array(
				  '#markup' => render($my_ill),
				),
				'form_wrapper_close' => array('#markup' => '</div>'),
				'#attached' => array
        (
          'js' => array(drupal_get_path("module", "ucsblib_library_search").'/js/customLogin.js'),
          'css' => array(drupal_get_path("module", "ucsblib_library_search").'/css/customLogin.css'),
        ),
  	  );
    break;
  }
  return $block;
}

/*
 * Online Archive of California Sidebar search form
 *
 */

function oac_search_form($form, &$form_state) {

  //Open in new window
  $form['#attributes'] = array('target' => '_blank');

  //OAC Search field
  $form['search_query'] = array(
    '#title' => t('Search'),
    '#title_display' => 'invisible',
    '#type' => 'textfield',
    '#size' => 20,
    '#attributes' => array('placeholder' => t('Search'))
  );

  //Submit Button
  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t(' '),
    '#attributes' => array('aria-label' => t('Search'))
  );

  return $form;
}

/*
 * Submit handler for Online Archive of California Sidebar search form
 *
 */

function oac_search_form_submit($form, &$form_state) {
	//URL Encode search String
	$query = check_url($form_state['values']['search_query']);

	//Third party search string
	$oac_path = 'https://oac.cdlib.org/search?&x=0&y=0&style=oac4&ff=0&developer=local&institution=UC+Santa+Barbara%3A%3ASpecial+Research+Collections&query=' . $query;

	//Set redirect to the Online Archive site
	$form_state['redirect'] = $oac_path;
}

/*
 * UCSB Library Search search form
 *
 */

function library_catalog_search_form($form, &$form_state) {

  //Open in new window
  $form['#attributes'] = array('target' => '_blank');

  //Wrapper for jQuery
  $form['#prefix'] = '<div class="hp-search-library-catalog">
<p class="hp-search-title">
UC Library Search
</p><div class="info-wrapper"><button aria-label="info" class="info_icon"></button><div class="info-content element-invisible"><div class="field-body info-body">Find books, articles, journals, media, and more.</div></div></div>';
  $form['#suffix'] = '</div>';

  //UC Library Search field
  $form['request'] = array(
    '#title' => t('Search articles, books, and more'),
    '#title_display' => 'invisible',
    '#type' => 'textfield',
    '#size' => 20,
    '#attributes' => array('placeholder' => t('Search articles, books, and more'))
  );

  //Submit Button
  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );

$form['advanced_search'] = array(
  	'#markup' => l(t('Advanced Search'),'https://search.library.ucsb.edu/discovery/search?vid=01UCSB_INST:UCSB&lang=en&mode=advanced'),
  	'#prefix' => '<div class="hp-search-extra-links">',
  	'#suffix' => '&nbsp; <span style="color: white;">&bull;</span> &nbsp;',
  );

$form['Browse Search'] = array(
  	'#markup' => l(t('Browse Search'),'https://search.library.ucsb.edu/discovery/browse?vid=01UCSB_INST:UCSB'),
  	'#prefix' => '&nbsp;',
  	'#suffix' => '</div>',
  );

  return $form;
}

/*
 * Submit handler for UCSB Library Search search form
 *
 */

function library_catalog_search_form_submit($form, &$form_state) {
	//URL Encode search String
	$query = check_url(rawurlencode($form_state['values']['request']));

	//Third party search string
	$library_catalog_path = 'https://search.library.ucsb.edu/discovery/search?query=any,contains,' . $query . '&tab=Everything&search_scope=DN_and_CI&vid=01UCSB_INST:UCSB&lang=en&offset=0';

	//Set redirect to the library discovery site
	$form_state['redirect'] = $library_catalog_path;
}

/*
 * Alexandria Digital Research Library Search Form
 *
 */

function alexandria_digital_research_library_search_form($form, &$form_state) {

  //Open in new window
  $form['#attributes'] = array('target' => '_blank');

  //Wrapper for jQuery
  $form['#prefix'] = '<div class="hp-search-alexandria"><p class="hp-search-title">Alexandria Digital Research Library</p> <div class="info-wrapper"><button aria-label="info" class="info_icon"></button><div class="info-content element-invisible"><div class="field-body info-body">Find unique research materials held in the UC Library, including images, audio, theses & dissertations, and more.</div></div></div>';
  $form['#suffix'] = '</div>';

  //ADRL Search field
  $form['q'] = array(
    '#title' => t('Search local digital collections'),
    '#title_display' => 'invisible',
    '#type' => 'textfield',
    '#size' => 20,
    '#attributes' => array('placeholder' => t('Search local digital collections'))
  );

  //Search criteria dropdown
  $form['search_field'] = array(
    '#title' => t('Search Criteria'),
    '#title_display' => 'invisible',
    '#type' => 'select',
    '#options' => array(
      'all_fields' => t('All Fields'),
      'title' => t('Title'),
      'subject' => t('Subject'),
      'accession_number' => t('Accession Number'),
    ),
    '#select2' => array(),
  );

  //Submit Button
  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );

  $form['browse_by_collection'] = array(
  	'#markup' => l(t('Browse by Collection'),'https://alexandria.ucsb.edu/collections'),
  	'#prefix' => '<div class="hp-search-extra-links">',
  	'#suffix' => '</div>',
  );

  return $form;
}

/*
 * Submit handler for Alexandria Digital Research Library  (ADRL) Search Form
 *
 */

function alexandria_digital_research_library_search_form_submit($form, &$form_state) {
	//URL Encode search String
	$query = check_url($form_state['values']['q']);
	$criteria = check_url($form_state['values']['search_field']);

	//Third party search string
	$alexandria_digital_research_library = 'https://alexandria.ucsb.edu/catalog?q=' . $query . '&search_field=' . $criteria;

	//Set redirect to the Alexandria Digital Research Library site
	$form_state['redirect'] = $alexandria_digital_research_library;
}

/*
 * Course Reserves Search Form
 * used to be Melvyl Catalog Search Form
 *
 */

function melvyl_catalog_search_form($form, &$form_state) {

  //Open in new window
  $form['#attributes'] = array('target' => '_blank');

  //Wrapper for jQuery
  $form['#prefix'] = '<div class="hp-search-melvyl"><p class="hp-search-title">Course Reserves</p><div class="info-wrapper"><button aria-label="info" class="info_icon"></button><div class="info-content element-invisible"><div class="field-body info-body">Find course materials that your instructor has placed on reserve in UCSB Library.</div></div></div>';
  $form['#suffix'] = '</div>';

  // Course Reserves Search field
  $form['q'] = array(
    '#title' => t('Search physical reserves'),
    '#title_display' => 'invisible',
    '#type' => 'textfield',
    '#size' => 20,
    '#attributes' => array('placeholder' => t('Search physical reserves'))
  );

  //Submit Button
  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),

  );

  $form['advanced_search'] = array(
  	'#markup' => l(t('Electronic Reserves'),'https://reserves.library.ucsb.edu/'),
  	'#prefix' => '<div class="hp-search-extra-links">',
  	'#suffix' => '</div>',
  );

  return $form;
}

/*
 * Submit handler for Course Reserves Search Form
 *
 */

function melvyl_catalog_search_form_submit($form, &$form_state) {
  //URL Encode search String
    $query = $form_state['values']['search_criteria'] . ":" . check_url(rawurlencode($form_state['values']['q']));

  //Exlibris Primo Course Reserves search scope string
   $melvyl_catalog = 'https://search.library.ucsb.edu/discovery/search?query=any,contains,' . $query . ',AND&tab=CourseReserves&search_scope=CourseReserves&vid=01UCSB_INST:UCSB&lang=en&mode=advanced&offset=0';

  //Set redirect to the Primo VE site
    $form_state['redirect'] = $melvyl_catalog;
  }

/*
 * Research Guides Search Form
 *
 */

function research_guides_search_form($form, &$form_state) {

  //Open in new window
  $form['#attributes'] = array('target' => '_blank');

  //Wrapper for jQuery
  $form['#prefix'] = '<div class="hp-search-research-guides"><p class="hp-search-title">Research Guides</p><div class="info-wrapper"><button aria-label="info" class="info_icon"></button><div class="info-content element-invisible"><div class="field-body info-body">Find recommended sources for research in your subject.</div></div></div>';
  $form['#suffix'] = '</div>';

  //Research Guides Search field
  $form['q'] = array(
    '#title' => t('Find a research guide'),
    '#title_display' => 'invisible',
    '#type' => 'textfield',
    '#size' => 20,
    '#attributes' => array('placeholder' => t('Find a research guide'))
  );

  //Submit Button
  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );
  
    $form['advanced_search'] = array(
  	'#markup' => l(t('All Guides'),'https://guides.library.ucsb.edu/'),
  	'#prefix' => '<div class="hp-search-extra-links">',
  	'#suffix' => '</div>',
  );

  return $form;
}

/*
 * Submit handler for Research Guides Search Form
 *
 */

function research_guides_search_form_submit($form, &$form_state) {
	//URL Encode search String
	$query = check_url($form_state['values']['q']);

	$research_guides = 'https://guides.library.ucsb.edu/srch.php?q=' . $query;

	//Set redirect to the Melvyl Catalog site
	$form_state['redirect'] = $research_guides;
}

function research_databases_subject_search_form($form, &$form_state) {

	$view = views_get_view('research_databases');
	$view->set_display('subject_page');
	$view->init_handlers(); //initialize display handlers
	$form_state = array(
	  'view' => $view,
	  'display' => $view->display_handler->display,
	  'exposed_form_plugin' => $view->display_handler->get_plugin('exposed_form'), //exposed form plugins are used in Views 3
	  'method' => 'get',
	  'rerender' => TRUE,
	  'no_redirect' => TRUE,
	);
	$form = drupal_build_form('views_exposed_form', $form_state);

	$form['#prefix'] = '<div class="hp-research-databases"><p class="hp-search-title">Articles and Databases</p><div class="info-wrapper"><button aria-label="info" class="info_icon"></button><div class="info-content element-invisible"><div class="field-body info-body">Find scholarly journal articles, digitized primary sources, streaming media, and more.</div></div></div>';
	
	$form['#suffix'] = '</div>';

	$glossary = views_embed_view('research_databases', 'attachment_1');
  if ($glossary) {
    $form['#suffix'] = $glossary . '<div class="hp-search-extra-links"><a href="/research/resources/databases/advanced">Advanced Search</a></div></div>';
  }
  
  $form['subj']['#attributes']['class'][] = 'use-select-2 form-select';
  
	return $form;
}

/*
 * Online Archive of California homepage search form
 *
 */

function oac_homepage_search_form($form, &$form_state) {

  //Open in new window
  $form['#attributes'] = array('target' => '_blank');

  //Wrapper for jQuery
  $form['#prefix'] = '<div class="hp-search-oac"><p class="hp-search-title">Archives & Manuscripts</p><div class="info-wrapper"><button aria-label="info" class="info_icon"></button><div class="info-content element-invisible"><div class="field-body info-body">Find items held in the UCSB archives and manuscript collections, via the Online Archive of California.</div></div></div>';
  $form['#suffix'] = '</div>';

  //OAC Search field
  $form['search_query'] = array(
    '#title' => t('Search UCSB archival collections'),
    '#title_display' => 'invisible',
    '#type' => 'textfield',
    '#size' => 20,
    '#attributes' => array('placeholder' => t('Search UCSB archival collections'))
  );

  //Submit Button
  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );

  return $form;
}

/*
 * Submit handler for Online Archive of California homepage search form
 *
 */

function oac_homepage_search_form_submit($form, &$form_state) {
	//URL Encode search String
	$query = check_url($form_state['values']['search_query']);

	//Third party search string
	$oac_path = 'https://oac.cdlib.org/search?&x=0&y=0&style=oac4&ff=0&developer=local&institution=UC+Santa+Barbara&query=' . $query;

	//Set redirect to the Online Archive site
	$form_state['redirect'] = $oac_path;
}

/*
 * Off-Campus Login form
 *
 */

function off_campus_login_form($form, &$form_state) {

  $form['#action'] = 'https://login.proxy.library.ucsb.edu/login';

  //Wrapper for jQuery
  $form['#prefix'] = '<div class="custom-login-off-campus">';
  $form['#suffix'] = '</div>';

  //Username field
  $form['user'] = array(
    '#title' => t('UCSB NetID'),
    '#type' => 'textfield',
    '#size' => 10,
  );

  //Password field
  $form['pass'] = array(
    '#title' => t('Password'),
    '#type' => 'password',
    '#size' => 10,
  );

  //Submit Button
  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Sign In'),
  );

  $form['forgot_login'] = array(
  	'#markup' => l(t('Forgot your login?'),'https://secure.identity.ucsb.edu/manager/'),
  	'#prefix' => '<div>',
  	'#suffix' => '</div>',
  );

  return $form;
}

/*
 * My ILL Requests Login form
 *
 */

function my_ill_login_form($form, &$form_state) {

  $form['#action'] = 'https://ucill.vdxhost.com/zportal/zengine';

  //Wrapper for jQuery
  $form['#prefix'] = '<div class="custom-login-my-ill">';
  $form['#suffix'] = '</div>';

  //Username field
  $form['login_user'] = array(
    '#title' => t('Library Card/Account'),
    '#type' => 'textfield',
    '#size' => 10,
  );

  $form['VDXaction'] = array(
    '#type' => 'hidden',
    '#value' => 'Login',
  );

  $form['nextAction'] = array(
    '#type' => 'hidden',
    '#value' => 'IllSearchAdvanced',
  );

  $form['login_service_id'] = array(
    '#title' => t('Campus'),
    '#type' => 'select',
    '#options' => array(
      '7' => t('UCSB'),
      '2' => t('UCB'),
      '8' => t('UCD'),
      '14' => t('UCI'),
      '6' => t('UCLA'),
      '16' => t('UCM'),
      '12' => t('UCR'),
      '11' => t('UCSC'),
      '13' => t('UCSD'),
      '10' => t('UCSF'),
      '15' => t('xCDL'),
    ),
    '#select2' => array(),
  );

  //Submit Button
  $form['.x'] = array(
    '#type' => 'submit',
    '#value' => t('Sign In'),
  );

  $form['forgot_login'] = array(
  	'#markup' => l(t('Forgot your login?'),'/node/1352'),
  	'#prefix' => '<div>',
  	'#suffix' => '</div>',
  );

  return $form;
}

function ucsblib_library_search_form_alter(&$form, &$form_state, $form_id){
  if($form_id=='block_admin_configure' && !empty($form['module'])  && $form['module']['#value']=='ucsblib_library_search' &&
  $form['delta']['#value']=='homepage_search_block') {
    /*//dpm($form);
    $form['settings']['library_catalog']=array(
      '#type' => 'textarea',
      '#title' => 'library catalog information'
    );*/
  }
	//remove hidden fields from views filters on the home page, because they were getting
	//added to the querystring and breaking the view
	if ($form_id == 'research_databases_subject_search_form' && $form['#id'] == 'views-exposed-form-research-databases-subject-page'){
		unset($form['form_token']);
		unset($form['form_id']);
		unset($form['form_build_id']);
	}
}
