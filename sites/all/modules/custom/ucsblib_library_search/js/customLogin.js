(function ($) {
  Drupal.behaviors.customLogin = {
    attach: function (context, settings) {
      $('body').once('customLogin', function() {
				var $controls = $('ul.custom-login-controls a');
				var $active_form = $('.' + $('a.custom-login-links-active').attr('data-target'));
				$login = $('#custom-login');
				
				$(document).click(function() {
					$active_form.stop(true, true).slideUp( 250, function() {
					  $('.custom-login-block').stop(true, true).slideUp();
			  	});
					$login.removeClass('visible');
				});
				
				$login.click(function(e) {
					e.preventDefault();
					e.stopPropagation();
					if ($(this).hasClass('visible')) {
						$active_form.stop(true, true).slideUp( 250, function() {
						  $('.custom-login-block').stop(true, true).slideUp();
				  	});
						$(this).removeClass('visible');
					} else {
						$('.custom-login-block').stop(true, true).slideDown();
						$(this).addClass('visible');
					}
				});
				$('.custom-login-block').click(function(e){
					e.stopPropagation();
				});
				
				
				
				$controls.click(function(e) {
				  if ($(this).hasClass('custom-login-form')) {						  
						$('a.custom-login-links-active').removeClass('custom-login-links-active');
						$(this).addClass('custom-login-links-active');
						$active_form.stop(true, true).slideUp( 250, function() {
						  $active_form = $('.' + $('a.custom-login-links-active').attr('data-target'));
						  $active_form.stop(true, true).slideDown(250);
				  	});
				  }
				});
			});
    }
  };
})(jQuery);
