(function ($) {
  Drupal.behaviors.thirdPartySearchBlocks = {
    attach: function (context, settings) {
      //info button
 
      // info icon tooltip for touch/mobile
      var $clicked=0;
      $('.info_icon').on('mousedown touchstart', function(){
        $(this).siblings().removeClass('element-invisible');
        var $container_left_pos=$(this).siblings().offset().left;
        var $container_width= $(this).siblings().width();

        if(!$clicked && ($(window).width()<=$container_left_pos+$container_width || $container_left_pos-$container_width<=0)){
          var $shift_left=($container_left_pos + $container_width - $(window).width())*-1;
          $(this).siblings().css('left',$shift_left+"px");
          $clicked=1;
        }
        /*
        else if($container_left_pos-$container_width<=0) {
          var $shift_left=($container_left_pos + $container_width - $(window).width())*-1 ;
          $(this).siblings().css('left',$shift_left+"px");
        }*/

        $('html').on('mousedown touchstart', function(){
          if ($(event.target).parents('.info_icon').length==0 && !$(event.target).hasClass("info_icon") && !$(event.target).hasClass("info-content")
          && !$(event.target).hasClass('info-body')) {
            $(".info-content").addClass('element-invisible');
            $clicked=0;
            $('.info-content').removeAttr("style");
            $(this).unbind(event);
          }
        });
      });
      
      // info icon tooltip for desktop
      $('.info_icon').click(function() {
        $(this).siblings().removeClass('element-invisible');
        var $container_left_pos=$(this).siblings().offset().left;
        var $container_width= $(this).siblings().width();

        if(!$clicked && ($(window).width()<=$container_left_pos+$container_width || $container_left_pos-$container_width<=0)){
          var $shift_left=($container_left_pos + $container_width - $(window).width())*-1;
          $(this).siblings().css('left',$shift_left+"px");
          //prevents the icon from being spam clicked
          $clicked=1;
          
        }
        /*
        else if($container_left_pos-$container_width<=0) {
          var $shift_left=($container_left_pos + $container_width - $(window).width())*-1 ;
          $(this).siblings().css('left',0+"px");
        }
        */
        $('html').click( function(){
          if ($(event.target).parents('.info_icon').length==0 && !$(event.target).hasClass("info_icon") && !$(event.target).hasClass("info-content")
          && !$(event.target).hasClass('info-body')) {
            $(".info-content").addClass('element-invisible');
            $clicked=0;
            $('.info-content').removeAttr("style");
            $(this).unbind(event);
            
          }
        });
      });
      //$('.info_icon').blur(function(event) {
      //    $(".info-content").addClass('element-invisible');
      //});
      
      
      $('body').once('thirdPartySearchBlocks', function() {

				var $controls = $('ul.hp-search-controls a');
				var $active_form = $('.' + $('a.hp-search-links-active').attr('data-target'));
				var $overlay = $('div.hp-search-controls-wrapper div.overlay');
				var $lastClicked = $('ul.hp-search-controls li:eq(0) a');
				$mobileSelect = $("select[name=hp-mobile-list]");
				$controls.matchHeight({ byRow: false }); 
				
				$controls.each(function(){
					var spanHeight = $(this).find('span').height();
					$(this).find('span').css({ 'padding-top': ($(this).height() - spanHeight) / 2 });
				});
				
				$(window).resize(function(){
					$controls.each(function(){
						var spanHeight = $(this).find('span').height();
						$(this).find('span').css({ 'padding-top': ($(this).height() - spanHeight) / 2 });
					});
					var $lastClickedOffset = $lastClicked.position();
					$overlay.css({'top': $lastClickedOffset.top - 5, 'left': $lastClickedOffset.left + 11}).height($('ul.hp-search-controls li:eq(0) a').height() + 10).width($('ul.hp-search-controls li:eq(0) a').outerWidth()).find('span').css({ 'padding-top': ($overlay.height() - $overlay.find('span').height()) / 2 });
				});
				
				$overlay.height($('ul.hp-search-controls li:eq(0) a').height() + 10).width($('ul.hp-search-controls li:eq(0) a').outerWidth()).find('span').css({ 'padding-top': ($overlay.height() - $overlay.find('span').height()) / 2 });
				
				$mobileSelect.change(function() {
          let selectedValue = $(this).val();
          if (selectedValue === 'hp-research-databases') {
            window.open("https://guides.library.ucsb.edu/az/databases","_self")
          } else {
            $('ul.hp-search-controls a[data-target=' + selectedValue + ']').click();
          }
				});
				
				$overlay.find('a').click(function(e) {
 				  	e.preventDefault();
 				} );
				
				$controls.click(function(e) {

          // Check if the clicked element has the ID 'springshare-db-list'
           if ($(this).attr('id') === 'springshare-db-list') {
            window.open("https://guides.library.ucsb.edu/az/databases","_self")
            return; // Do nothing if the ID matches
           }
				  e.preventDefault();
				  $lastClicked = $(this);
				  var offset = $(this).position();
				  var $clicked = $(this);
				  
				  $('a.hp-search-links-active').removeClass('hp-search-links-active');
				  $(this).addClass('hp-search-links-active');
				  $active_form.fadeOut( 250, function() {
				    $active_form = $('.' + $('a.hp-search-links-active').attr('data-target'));
				    $mobileSelect.val($('a.hp-search-links-active').attr('data-target'));
				    $active_form.fadeIn(250);
				    $overlay.find('span').text($clicked.text()).css({ 'padding-top': ($overlay.height() - $overlay.find('span').height()) / 2 });
						$overlay.css({'width': $clicked.outerWidth() + 5 }).stop(true, true).css({ left: offset.left + 6, top: offset.top - 5});
		  		});
				});
	  	});
    }
  };
})(jQuery);
