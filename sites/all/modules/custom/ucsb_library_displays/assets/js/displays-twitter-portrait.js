(function ($) {
  Drupal.behaviors.displaysTwitterPortraitLibrary = {
    attach: function (context, settings) {

      $('.flexslider').on('start', function (slider) {
        $('.flexslider li').each(function(){
          var liHeight = $(this).height();
          var tweetTopSide = $(this).find('.tweet-top-side').height();
          var fieldBody = $(this).find('.field-body').height();
          var tweetPortraitItemHeight = $(this).find('.tweet-portrait-item').outerHeight();
          var tweetPortraitItemMaxHeight = $(this).find('.tweet-portrait-item').css('max-height');
          var remainingSpace = liHeight - tweetPortraitItemHeight;
          var marginTopBottom = remainingSpace / 2;

          var fieldTwitterMediaMaxHeight = parseInt(tweetPortraitItemMaxHeight) - tweetTopSide - fieldBody - 44 * 2;
          $(this).find('.field-twitter-media img').css({'max-height' : fieldTwitterMediaMaxHeight});
          $(this).find('.views-field-nothing').css({'padding-top' : marginTopBottom, 'padding-bottom' : marginTopBottom});
        });
      });
    }
  };

}) (jQuery);
