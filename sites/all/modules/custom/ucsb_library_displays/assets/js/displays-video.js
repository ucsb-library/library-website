(function ($) {
  Drupal.behaviors.displaysVideoLibrary = {
    attach: function (context, settings) {

      $('.flexslider').on('start', function (slider) {
        var video = $('li.flex-active-slide video')[0];
        video.play();

        $('.flexslider').flexslider("pause");
        //when the video has ended, go to the next slide and play the slider
        video.onended = function(e){
          $('.flexslider').flexslider("next");
          $('.flexslider').flexslider("play");
        };
      });

      $('.flexslider').on('before', function (slider) {
          $(this).find('video').each(function() {
            $(this).get(0).load();
          });
      });

      $('.flexslider').on('after', function (slider) {
        var video = $('li.flex-active-slide video')[0];

        video.play();

        //when the video has ended, go to the next slide and play the slider
        video.onended = function(e){
          $('.flexslider').flexslider("next");
          $('.flexslider').flexslider("play");
        };

        $('.flexslider').flexslider("pause");
      });

    }
  };

}) (jQuery);
