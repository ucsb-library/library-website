<?php

  $dates = $node->field_event_date[LANGUAGE_NONE];
  $date = $dates[0];

  $yesterday = format_date(time() - 24 * 3600, 'custom', 'Ymd');
  $tmp_date = NULL;

  // Set title.
  $title = NULL;
  if (!empty($node->field_display_title[LANGUAGE_NONE][0]['value'])) {
    $title = $node->field_display_title[LANGUAGE_NONE][0]['value'];
  }

  // Set Body.
  $description = NULL;
  if (!empty($node->field_display_description[LANGUAGE_NONE][0]['value'])) {
    $description = $node->field_display_description[LANGUAGE_NONE][0]['value'];
  }

  // Set Location.
  $location = NULL;
  if (isset($node->field_location[LANGUAGE_NONE][0])) {
    $tmp_location = [];
    foreach ($node->field_location[LANGUAGE_NONE] as $delta => $item) {
      $tmp_location[] = $item['taxonomy_term']->name;
    }
    $location = implode(", ", $tmp_location);
  }

  // Set Dates.
  if (count($dates) > 1) {
    foreach ($dates as $delta => $item) {
      $item_start_date_unixdate = strtotime($item['value'] . ' ' . $item['timezone_db']);
      $item_start_date = format_date($item_start_date_unixdate,'custom','Y-m-d H:i:s', $item['timezone']);
      $item_start_date_day = format_date($item_start_date_unixdate,'custom','Ymd', $item['timezone']);

      $item_end_date_unixdate = strtotime($item['value2'] . ' ' . $item['timezone_db']);
      $item_end_date = format_date($item_end_date_unixdate,'custom','Y-m-d H:i:s', $item['timezone']);
      $item_end_date_day = format_date($item_end_date_unixdate,'custom','Ymd', $item['timezone']);

      // Event Date > now -1 day
      if ( ( $item_start_date_day > $yesterday ) || ( $item_end_date_day > $yesterday ) ) {
        if ($tmp_date) {
          $tmp_date_unixdate = strtotime($tmp_date['value'] . ' ' . $tmp_date['timezone_db']);
          $tmp_date_day = format_date($tmp_date_unixdate, 'custom', 'Ymd', $tmp_date['timezone']);

          if ($item_start_date_day < $tmp_date_day) {
            $tmp_date = $date = $item;
          }
          else {
            $date = $tmp_date;
          }
        }
        else {
          $tmp_date = $date = $item;
        }
      }
    }
  }
  if ($date['value'] == $date['value2']) {
    $start_date_unixdate = $end_date_unixdate = strtotime($date['value'] . ' ' . $date['timezone_db']);
    $start_date = $end_date = format_date($start_date_unixdate,'custom','Y-m-d H:i:s', $date['timezone']);
  }
  else {
    $start_date_unixdate = strtotime($date['value'] . ' ' . $date['timezone_db']);
    $start_date = format_date($start_date_unixdate,'custom','Y-m-d H:i:s', $date['timezone']);

    $end_date_unixdate = strtotime($date['value2'] . ' ' . $date['timezone_db']);
    $end_date = format_date($end_date_unixdate,'custom','Y-m-d H:i:s', $date['timezone']);
  }
  $month = format_date($end_date_unixdate, 'custom', 'M');
  $day = format_date($end_date_unixdate, 'custom', 'j');

  // Set UCSB Reads.
  $ucsb_reads = FALSE;
  if (isset($node->field_series[LANGUAGE_NONE]) && $node->field_series[LANGUAGE_NONE]) {
    foreach ($node->field_series[LANGUAGE_NONE] as $delta => $item) {
      if ($item['taxonomy_term']->name == 'UCSB Reads') {
        $ucsb_reads = '<img alt="UCSB Reads" src="/' . drupal_get_path('module', 'ucsb_library_displays_events') . '/assets/images/ucsb-reads-' . date('Y') . '.png" />';
      }
    }
  }
?>
<div class="event-wrapper" data-nid="<?php print $node->nid; ?>">
  <div class="image-and-date-wrapper">
    <div class="date-wrapper">
      <span class="month"><?php print $month; ?></span>
      <span class="day"><?php print $day; ?></span>
    </div>
    <?php print render($content['field_event_image']); ?>
  </div>
  <div class="info-wrapper">
    <div class="info-wrapper-inner">
      <?php if ($ucsb_reads): ?>
        <div class="title-wrapper-reads">
          <h3 class="title">
            <?php print $title; ?>
          </h3>
          <div class="ucsb-reads ucsb-reads-portrait">
            <?php print $ucsb_reads; ?>
          </div>
          <div class="body reads-body">
            <?php print $description; ?>
          </div>
        </div>
      <?php else: ?>
        <h3 class="title">
          <?php print $title; ?>
        </h3>
        <div class="body">
          <?php print $description; ?>
        </div>
      <?php endif; ?>
      <div class="date">
        <span class="countdown-wrapper"></span>
        <span class="start-date" data-countdown="<?php print $start_date; ?>"></span>
        <span class="end-date" data-countdown="<?php print $end_date; ?>"></span>
      </div>

      <?php if ($location): ?>
      <div class="location">
        <?php print $location; ?>
      </div>
      <?php endif; ?>

      <div class="library-signature">
        <?php print '<img alt="UC SANTA BARBARA Library" src="/' . drupal_get_path('module', 'ucsb_library_displays_events') . '/assets/images/uc-santa-barbara-logo.png" />'; ?>
      </div>

      <?php if ($ucsb_reads): ?>
        <div class="ucsb-reads ucsb-reads-landscape">
          <?php print $ucsb_reads; ?>
        </div>
      <?php endif; ?>
    </div>
  </div>
</div>
