<?php

  // currently, running once per day
  $last_run_date = variable_get('kwall_migration_cron_last_run', FALSE);

  if (date('Ymd') > $last_run_date || !$last_run_date) {

    $remote_file_url = variable_get('kwall_migration_url');  
    $orig_xml_data = file_get_contents($remote_file_url);  
    $new_file_data = str_replace(' xmlns="http://www.w3.org/2005/Atom"', "", $orig_xml_data);

    $main_application_file = file_save_data($new_file_data, "private://kwall_migrate/kwall_migrate.xml", FILE_EXISTS_REPLACE);

    if(!empty($main_application_file->uri)) {
      // set variable on success
      variable_set('kwall_migration_cron_last_run', date('Ymd', REQUEST_TIME));

      // watchdog success
      watchdog('kwall_migration', t('Successfully downloaded XML file.'));
    }
    else {
      // watchdog error
      watchdog('kwall_migration', t("Error downloading file."), array(), array(), WATCHDOG_ERROR);
    }
  }