<?php


/* ------------------------------ School Wide Events Migration Below ---------------------------------*/

class KwallMigrate extends DrupalNode6Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Namespaced XML feed of KWALL Events');

    $fields = array(
      'id' => t('Event ID'),
      'title' => t('Event Title')
    );

/*
    $url = variable_get('kwall_migration_url');
    if (!empty($url)) {
      //$list_url = $url;
    }
    else {
      $list_url = '0';
    }
*/
     //$list_url = 'http://25livepub.collegenet.com/calendars/drupal-main-calendar-feed.xml';
//     $xml_folder = DRUPAL_ROOT . '/' . drupal_get_path('module', 'ssuedu_event_migration') . '/';
    //$items_url = $xml_folder . 'accounting-classes.rss';
  
    $source_url = variable_get('kwall_migration_url');
    $orig_xml_data = file_get_contents($source_url);
    
    $new_file_data = str_replace(' xmlns="http://www.w3.org/2005/Atom"', "", $orig_xml_data);
  
    $new_folder = 'private://kwall_migrate/';
    file_prepare_directory($new_folder, FILE_CREATE_DIRECTORY);
    
    $main_application_file = file_save_data($new_file_data, $new_folder . 'kwall_migrate.xml', FILE_EXISTS_REPLACE);


    // We use the MigrateSourceList class for any source where we obtain the
    // list of IDs to process separately from the data for each item. The
    // listing and item are represented by separate classes, so for example we
    // could replace the XML listing with a file directory listing, or the XML
    // item with a JSON item.
    
    $items_class = new MigrateItemsXML($main_application_file->uri,$item_xpath, $item_ID_xpath);
    $this->source = new MigrateSourceMultiItems($items_class, $fields,array(''), $namespaces);
     
     
    $this->destination = new MigrateDestinationNode('event');

    // The source ID here is the one retrieved from the XML listing file, and
    // used to identify the specific item's file
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'id' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'description' => 'Event ID'
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    // TIP: Note that for XML sources, in addition to the source field passed to
    // addFieldMapping (the name under which it will be saved in the data row
    // passed through the migration process) we specify the Xpath used to
    // retrieve the value from the XML.
    
    $this->addFieldMapping('title','title');

    $this->addFieldMapping('body', 'body');
    $this->addFieldMapping('body:format')
      ->defaultValue('full_html');
    $this->addFieldMapping('body:language')
      ->defaultValue('en');

    $this->addFieldMapping('field_date_events', 'date');

    $this->addFieldMapping('field_event_categories', 'categories')->separator(',');
    $this->addFieldMapping('field_event_categories:create_term')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_event_categories:ignore_case')
      ->defaultValue(TRUE);

    $this->addFieldMapping('field_admission_fees', 'admissions');
    $this->addFieldMapping('field_event_url', 'website');
    $this->addFieldMapping('field_location_event', 'location');
    $this->addFieldMapping('field_contact_email', 'email');

    $this->addFieldMapping('field_sponsor', 'sponser');
    $this->addFieldMapping('field_contact_phone', 'phone');
    $this->addFieldMapping('field_ticket_url', 'ticket');
    $this->addFieldMapping('field_open_to_the_public', 'open_to_public');   
    
    $this->addFieldMapping('field_25_live')
      ->defaultValue(1);    
    $this->addFieldMapping('uid')
      ->defaultValue(1);
    $this->addFieldMapping('status')
      ->defaultValue(TRUE);
    $this->addFieldMapping('language')
      ->defaultValue('en');
      
    $destination_fields = $this->destination->fields();
    
    if (isset($destination_fields['path'])) {
      $this->addFieldMapping('path')
           ->issueGroup(t('DNM'));
      if (isset($destination_fields['pathauto'])) {
        $this->addFieldMapping('pathauto')
             ->issueGroup(t('DNM'));
      }
    }
  }

  public function prepareRow($row) {
//     dpm($row);
// Comment this function out if the 'unpublish' section is un-commented

    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

  }


  // Uncomment This section to unpublish events that are no longer included in the RSS feed
  // Needs to be done when events are no longer manually created on the site.
/*
  	public function complete($row) {
		if (!empty($row->field_uid)) {
			//dpm($row);
	      $this->event_nodes[] = $row->nid;
	    }
	}


  public function postImport() {
    parent::postImport();
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'event')
      ->propertyCondition('status', 1)

      ->addMetaData('account', user_load(1));
    if (!empty($this->event_nodes)) {
      $query->propertyCondition('nid', $this->event_nodes, 'NOT IN');

      $result = $query->execute();
      if (isset($result['node'])) {
        $nids = array_keys($result['node']);
        $queue = DrupalQueue::get('millikin_events_unpublish_node');

        foreach($nids as $nid) {
          $queue->createItem($nid);
        }
      }
    }
  }
*/
}