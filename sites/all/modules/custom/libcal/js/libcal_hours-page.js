(function ($, Drupal, window, document, undefined) {
  Drupal.behaviors.libcal_hours = {
    attach: function(libcal_hours, settings) {
      $('body').once('libcal_hours', function() {
	      var indexMatch = $('.hours-table-wrap').length - 1;
	      $('.hours-nav-title').text($('.active-week').attr('data-week'));
	      $('.hours-nav-right a').click(function(e){
		      e.preventDefault();
		      if ($('.active-week').index() < indexMatch) {
		      	$('.active-week').removeClass('active-week').hide().next().css({'display':'table'}).addClass('active-week');
		      	$('.hours-nav-title').text($('.active-week').attr('data-week'));
		      }
		      
		      if ($('.active-week').index() === indexMatch) {
			      $(this).parent().hide();
		      } else {
			      $(this).parent().show();
		      }
		      
		      if ($('.active-week').index() > 0) {
			      $('.hours-nav-left').show();
		      }
	      });
	      $('.hours-nav-left a').click(function(e){
		      e.preventDefault();
		      if ($('.active-week').index() > 0) {
		      	$('.active-week').removeClass('active-week').hide().prev().css({'display':'table'}).addClass('active-week');
		      	$('.hours-nav-title').text($('.active-week').attr('data-week'));
		      }
		      
		      if ($('.active-week').index() > 0) {
			      $(this).parent().show();
		      } else {
			      $(this).parent().hide();
		      }
		      
		      if ($('.active-week').index() < indexMatch) {
			      $('.hours-nav-right').show();
			    }
	      });
	      $('.hours-table-row-weekend .hours-table-cell.hours-table-library-name, .hours-table-row-weekend .hours-table-cell.hours-table-secondary-weekday.hours-table-cell-Monday').click(function(){
					if ($(this).parent().hasClass('open')) {
						$(this).parent().removeClass('open');
					} else {
						$(this).parent().addClass('open');
					}
				});
				$('.hours-table-row-primary .hours-table-cell.hours-table-library-name').click(function(){
					if ($(window).width() <= 800) {
						if ($(this).hasClass('open')) {
							$(this).parent().next('.hours-table-row.hours-table-row-weekend').fadeOut(100);
							$(this).removeClass('open');
						} else {
							$(this).parent().next('.hours-table-row.hours-table-row-weekend').css({'display':'table-row'});
							$(this).addClass('open');
						}
					}
				});
      });
    }
  };
})(jQuery, Drupal, this, this.document);