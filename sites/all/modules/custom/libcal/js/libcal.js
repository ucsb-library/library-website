(function ($, Drupal, window, document, undefined) {
  Drupal.behaviors.libcal = {
    attach: function(libcal, settings) {
      $('body').once('libcal', function() {
	      $footer_action = $('span.hours-dropdown-footer-action');
	      $('.hours-dropdown-footer-link button').click(function(e){
		      $('div.hours-dropdown-body').stop(true, true).slideToggle(250, function() {
						if (!$(this).hasClass('hours-open')) {
							$(this).addClass('hours-open');
							$footer_action.text('CLOSE X');
						} else {
							$(this).removeClass('hours-open');
							$footer_action.text('SEE MORE HOURS');
						}
  				});
	      });
      });
    }
  };
})(jQuery, Drupal, this, this.document);