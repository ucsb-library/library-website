<?php

/**
 * @file
 * ucsblde.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ucsb_library_displays_events_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'library_display_events';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Library Display - Events';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Library Display';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['query']['options']['pure_distinct'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'flexslider';
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['view_mode'] = 'display_teaser';
  $handler->display->display_options['row_options']['links'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Event Date -  start date (field_event_date) */
  $handler->display->display_options['sorts']['field_event_date_value']['id'] = 'field_event_date_value';
  $handler->display->display_options['sorts']['field_event_date_value']['table'] = 'field_data_field_event_date';
  $handler->display->display_options['sorts']['field_event_date_value']['field'] = 'field_event_date_value';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Display Type (field_display_type) */
  $handler->display->display_options['filters']['field_display_type_value']['id'] = 'field_display_type_value';
  $handler->display->display_options['filters']['field_display_type_value']['table'] = 'field_data_field_display_type';
  $handler->display->display_options['filters']['field_display_type_value']['field'] = 'field_display_type_value';
  $handler->display->display_options['filters']['field_display_type_value']['operator'] = 'not';
  $handler->display->display_options['filters']['field_display_type_value']['value'] = array(
    'portrait' => 'portrait',
  );
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'event_exhibition' => 'event_exhibition',
  );

  /* Display: Page - Events - Landscape */
  $handler = $view->new_display('page', 'Page - Events - Landscape', 'page_events_landscape');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Library Display Video - Landscape';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'flexslider';
  $handler->display->display_options['style_options']['optionset'] = 'events_landscape';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['view_mode'] = 'display_teaser';
  $handler->display->display_options['row_options']['links'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['filter_groups']['groups'] = array(
    1 => 'AND',
    2 => 'OR',
  );
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Display Type (field_display_type) */
  $handler->display->display_options['filters']['field_display_type_value']['id'] = 'field_display_type_value';
  $handler->display->display_options['filters']['field_display_type_value']['table'] = 'field_data_field_display_type';
  $handler->display->display_options['filters']['field_display_type_value']['field'] = 'field_display_type_value';
  $handler->display->display_options['filters']['field_display_type_value']['operator'] = 'not';
  $handler->display->display_options['filters']['field_display_type_value']['value'] = array(
    'portrait' => 'portrait',
  );
  $handler->display->display_options['filters']['field_display_type_value']['group'] = 1;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'event_exhibition' => 'event_exhibition',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Exclude Event from Display (field_exclude_event_from_display) */
  $handler->display->display_options['filters']['field_exclude_event_from_display_value']['id'] = 'field_exclude_event_from_display_value';
  $handler->display->display_options['filters']['field_exclude_event_from_display_value']['table'] = 'field_data_field_exclude_event_from_display';
  $handler->display->display_options['filters']['field_exclude_event_from_display_value']['field'] = 'field_exclude_event_from_display_value';
  $handler->display->display_options['filters']['field_exclude_event_from_display_value']['operator'] = 'not';
  $handler->display->display_options['filters']['field_exclude_event_from_display_value']['value'] = array(
    1 => '1',
  );
  $handler->display->display_options['filters']['field_exclude_event_from_display_value']['group'] = 1;
  /* Filter criterion: Content: Event Date -  start date (field_event_date) */
  $handler->display->display_options['filters']['field_event_date_value']['id'] = 'field_event_date_value';
  $handler->display->display_options['filters']['field_event_date_value']['table'] = 'field_data_field_event_date';
  $handler->display->display_options['filters']['field_event_date_value']['field'] = 'field_event_date_value';
  $handler->display->display_options['filters']['field_event_date_value']['operator'] = '>';
  $handler->display->display_options['filters']['field_event_date_value']['group'] = 1;
  $handler->display->display_options['filters']['field_event_date_value']['form_type'] = 'date_text';
  $handler->display->display_options['filters']['field_event_date_value']['default_date'] = 'now -1 day';
  $handler->display->display_options['filters']['field_event_date_value']['year_range'] = '-0:+3';
  $handler->display->display_options['filters']['field_event_date_value']['add_delta'] = 'yes';
  /* Filter criterion: Content: Event Date -  start date (field_event_date) */
  $handler->display->display_options['filters']['field_event_date_value_1']['id'] = 'field_event_date_value_1';
  $handler->display->display_options['filters']['field_event_date_value_1']['table'] = 'field_data_field_event_date';
  $handler->display->display_options['filters']['field_event_date_value_1']['field'] = 'field_event_date_value';
  $handler->display->display_options['filters']['field_event_date_value_1']['operator'] = '<=';
  $handler->display->display_options['filters']['field_event_date_value_1']['group'] = 2;
  $handler->display->display_options['filters']['field_event_date_value_1']['granularity'] = 'minute';
  $handler->display->display_options['filters']['field_event_date_value_1']['form_type'] = 'date_text';
  $handler->display->display_options['filters']['field_event_date_value_1']['default_date'] = 'now +2 week';
  $handler->display->display_options['filters']['field_event_date_value_1']['add_delta'] = 'yes';
  /* Filter criterion: Content: Event Date - end date (field_event_date:value2) */
  $handler->display->display_options['filters']['field_event_date_value2']['id'] = 'field_event_date_value2';
  $handler->display->display_options['filters']['field_event_date_value2']['table'] = 'field_data_field_event_date';
  $handler->display->display_options['filters']['field_event_date_value2']['field'] = 'field_event_date_value2';
  $handler->display->display_options['filters']['field_event_date_value2']['operator'] = '<=';
  $handler->display->display_options['filters']['field_event_date_value2']['group'] = 2;
  $handler->display->display_options['filters']['field_event_date_value2']['granularity'] = 'minute';
  $handler->display->display_options['filters']['field_event_date_value2']['form_type'] = 'date_text';
  $handler->display->display_options['filters']['field_event_date_value2']['default_date'] = 'now +2 week';
  $handler->display->display_options['filters']['field_event_date_value2']['add_delta'] = 'yes';
  $handler->display->display_options['path'] = 'displays/events-landscape';

  /* Display: Page - Events - Portrait */
  $handler = $view->new_display('page', 'Page - Events - Portrait', 'page_events_portrait');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Library Display Events - Portrait';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'flexslider';
  $handler->display->display_options['style_options']['optionset'] = 'events_portrait';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['view_mode'] = 'display_teaser';
  $handler->display->display_options['row_options']['links'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['filter_groups']['groups'] = array(
    1 => 'AND',
    2 => 'OR',
  );
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Display Type (field_display_type) */
  $handler->display->display_options['filters']['field_display_type_value']['id'] = 'field_display_type_value';
  $handler->display->display_options['filters']['field_display_type_value']['table'] = 'field_data_field_display_type';
  $handler->display->display_options['filters']['field_display_type_value']['field'] = 'field_display_type_value';
  $handler->display->display_options['filters']['field_display_type_value']['operator'] = 'not';
  $handler->display->display_options['filters']['field_display_type_value']['value'] = array(
    'landscape' => 'landscape',
  );
  $handler->display->display_options['filters']['field_display_type_value']['group'] = 1;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'event_exhibition' => 'event_exhibition',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Exclude Event from Display (field_exclude_event_from_display) */
  $handler->display->display_options['filters']['field_exclude_event_from_display_value']['id'] = 'field_exclude_event_from_display_value';
  $handler->display->display_options['filters']['field_exclude_event_from_display_value']['table'] = 'field_data_field_exclude_event_from_display';
  $handler->display->display_options['filters']['field_exclude_event_from_display_value']['field'] = 'field_exclude_event_from_display_value';
  $handler->display->display_options['filters']['field_exclude_event_from_display_value']['operator'] = 'not';
  $handler->display->display_options['filters']['field_exclude_event_from_display_value']['value'] = array(
    1 => '1',
  );
  $handler->display->display_options['filters']['field_exclude_event_from_display_value']['group'] = 1;
  /* Filter criterion: Content: Event Date -  start date (field_event_date) */
  $handler->display->display_options['filters']['field_event_date_value']['id'] = 'field_event_date_value';
  $handler->display->display_options['filters']['field_event_date_value']['table'] = 'field_data_field_event_date';
  $handler->display->display_options['filters']['field_event_date_value']['field'] = 'field_event_date_value';
  $handler->display->display_options['filters']['field_event_date_value']['operator'] = '>=';
  $handler->display->display_options['filters']['field_event_date_value']['group'] = 1;
  $handler->display->display_options['filters']['field_event_date_value']['form_type'] = 'date_text';
  $handler->display->display_options['filters']['field_event_date_value']['default_date'] = 'now -1 day';
  /* Filter criterion: Content: Event Date -  start date (field_event_date) */
  $handler->display->display_options['filters']['field_event_date_value_1']['id'] = 'field_event_date_value_1';
  $handler->display->display_options['filters']['field_event_date_value_1']['table'] = 'field_data_field_event_date';
  $handler->display->display_options['filters']['field_event_date_value_1']['field'] = 'field_event_date_value';
  $handler->display->display_options['filters']['field_event_date_value_1']['operator'] = '<=';
  $handler->display->display_options['filters']['field_event_date_value_1']['group'] = 2;
  $handler->display->display_options['filters']['field_event_date_value_1']['form_type'] = 'date_text';
  $handler->display->display_options['filters']['field_event_date_value_1']['default_date'] = 'now +2 week';
  /* Filter criterion: Content: Event Date - end date (field_event_date:value2) */
  $handler->display->display_options['filters']['field_event_date_value2']['id'] = 'field_event_date_value2';
  $handler->display->display_options['filters']['field_event_date_value2']['table'] = 'field_data_field_event_date';
  $handler->display->display_options['filters']['field_event_date_value2']['field'] = 'field_event_date_value2';
  $handler->display->display_options['filters']['field_event_date_value2']['operator'] = '<=';
  $handler->display->display_options['filters']['field_event_date_value2']['group'] = 2;
  $handler->display->display_options['filters']['field_event_date_value2']['form_type'] = 'date_text';
  $handler->display->display_options['filters']['field_event_date_value2']['default_date'] = 'now +2 week';
  $handler->display->display_options['path'] = 'displays/events-portrait';
  $export['library_display_events'] = $view;

  return $export;
}
