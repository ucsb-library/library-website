(function ($) {
  Drupal.behaviors.displaysEventsPortraitLibrary = {
    attach: function (context, settings) {

      // var timeout = 10000; // 5 seconds
      var timeout = 1000 * 3600 * 2; // 2 hours
      setTimeout(function() {
        location.reload();
      }, timeout);

      $('.flexslider').on('start', function (slider) {

        $('.flexslider li').each(function(index, value){
          var liHeight = $(this).height();
          var eventsLandscapeItemHeight = $(this).find('.views-field-nothing').outerHeight();
          var remainingSpace = liHeight - eventsLandscapeItemHeight;
          var marginTopBottom = remainingSpace / 2;
          $(this).find('.views-field-nothing').css({'padding-top' : marginTopBottom, 'padding-bottom' : marginTopBottom});

          if (! $(this).hasClass('clone')) {
            // if (index) {
            switch (index % 4) {
              case 1:
                $(this).addClass("color-aqua");
                break;
              case 2:
                $(this).addClass("color-sea-green");
                break;
              case 3:
                $(this).addClass("color-moss");
                break;
              case 0:
                $(this).addClass("color-coral");
                break;
            }
            // }
          }
        });
      });

      $('[data-countdown]').each(function() {
        var $this = $(this).parent().find('.start-date');

        var nowMoment = moment();

        $this.countdown($(this).parent().find('.start-date').data('countdown'), {elapse: true, defer: false})
          .on('update.countdown', function (event) {
            var eventMoment = moment(event.currentTarget.dataset.countdown);
            var endMoment = moment($(this).closest('.date').find('.end-date').data('countdown'));
            if (! event.elapsed) {
              if ($(this).parent().find('.start-date').html() == $(this).parent().find('.end-date').html()) {
                if (event.offset.totalDays >= 6) {
                  $(this).html(eventMoment.format('dddd, MMMM D, h:mm A'));
                }
                else {
                  if (event.offset.totalHours >= 24) {
                    $(this).html(eventMoment.format('dddd [at] h:mm A'));
                  }
                  else {
                    if (eventMoment.format('YYYYMMDD') == nowMoment.format('YYYYMMDD')) {
                      if (event.offset.totalHours >= 1) {
                        $(this).html(eventMoment.format('[Today at] h:mm A'));
                      }
                      else {
                        $(this).html(eventMoment.format('[Begins in] ' + event.offset.totalMinutes + ' [minutes]'));
                      }
                    }
                    else {
                      $(this).html(eventMoment.format('dddd [at] h:mm A'));
                    }
                  }
                }
              }
            }
            else {
              // Event already started or passed.
              if ($(this).parent().find('.start-date').html() == $(this).parent().find('.end-date').html()) {
                // End Date = Start Date
                var minutesPassed = nowMoment.diff(eventMoment, 'minutes');
                if (minutesPassed <= 15) {
                  $(this).html('Started + ' + minutesPassed + ' minutes ago');
                }
                else {
                  $(this).html(eventMoment.format('[Today at] h:mm A'));
                }
              }
              else {
                // End Date != Start Date
                var minutesPassedEnd = nowMoment.diff(endMoment, 'minutes');
                if (minutesPassedEnd < 0) {
                  $(this).html(endMoment.format('[Now Until] h:mm A'));
                } else {
                  $(this).html(eventMoment.format('[Today at] h:mm A'));
                }
              }
            }
          })
          .countdown('start');
      });

      // Add loaded class after JS processes
      setTimeout(function(){
        $('.page-displays .event-wrapper .info-wrapper .date').addClass('loaded');
      }, 1000);

    }
  };

}) (jQuery);
