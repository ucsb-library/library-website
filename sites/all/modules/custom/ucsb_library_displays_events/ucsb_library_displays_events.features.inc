<?php

/**
 * @file
 * ucsblde.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ucsb_library_displays_events_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "flexslider" && $api == "flexslider_default_preset") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function ucsb_library_displays_events_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function ucsblde_node_info() {
  $items = array(
    'event_exhibition' => array(
      'name' => t('Events & Exhibitions'),
      'base' => 'node_content',
      'description' => t('Use <em>Events & Exhibitions</em> to place items in the current Events & Exhibitions section or in the Exhibition Archive.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
