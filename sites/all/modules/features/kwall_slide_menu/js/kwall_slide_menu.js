(function ($, Drupal, window, document, undefined) {
  Drupal.behaviors.kwall_slide_menu = {
    attach: function(context, settings) {

      var navMenuID = Drupal.settings.kwall_slide_menu.navMenuID;
      var navSearchMenuID = Drupal.settings.kwall_slide_menu.navSearchMenuID;
      var $navMenu = $('#' + navMenuID);
      var $searchMenu = $('#' + navSearchMenuID);

      $navMenu.once('slideMenu', function() {
        // initialization
        var ESCAPE_CODE = 27;

        var $navAllLinks = $navMenu.find('a');
        var $navTopLevelLinks = $navAllLinks.not('ul.slide-nav-menu a');
        var $searchInputs = $navMenu.find('input:not([type="hidden"])');

        var menuOpenSelector = Drupal.settings.kwall_slide_menu.menuOpenSelector;
        var searchOpenSelector = Drupal.settings.kwall_slide_menu.searchOpenSelector;
        var $openButtons = $(menuOpenSelector + ',' + searchOpenSelector); // the buttons that open the nav and search
        var $navOpenButton = $(menuOpenSelector); // the buttons that open the nav
        var $searchOpenButton = $(searchOpenSelector); // the buttons that open the search
        var $clickedOpenButton = null; // keep track of which button opened the nav (menu or search link)

        function closeMenu() {
          $navMenu.removeClass('active').attr('aria-hidden');
          $searchMenu.removeClass('active').attr('aria-hidden');
          $openButtons.attr('aria-label', 'Search and menu collapsed');
          // Take all nav links and search inputs out of tab order
          $navAllLinks.attr('tabIndex', '-1');
          $searchInputs.attr('tabIndex', '-1');
          if ($clickedOpenButton) {
            // Set focus back to the button that opened the nav
            $clickedOpenButton.focus();
          }
        }

        // Initialize to closed state
        $openButtons.attr({'aria-controls': navMenuID, 'aria-haspopup': 'true'});
        closeMenu();
        $navMenu.find('ul.slide-nav-menu').attr('aria-hidden', 'true');

        function openNavMenu() {
          $navMenu.removeClass('closed').addClass('active').removeAttr('aria-hidden');
          $openButtons.attr('aria-label', 'Search and menu expanded');
          // Add all nav links and search inputs back to tab order
          $navTopLevelLinks.removeAttr('tabIndex');
          $searchInputs.removeAttr('tabIndex');
        }

        function openSearchMenu() {
          $searchMenu.removeClass('closed').addClass('active').removeAttr('aria-hidden');
          $openButtons.attr('aria-label', 'Search and menu expanded');
          // Add all nav links and search inputs back to tab order
          $navTopLevelLinks.removeAttr('tabIndex');
          $searchInputs.removeAttr('tabIndex');
        }

        $navMenu.bind("webkitTransitionEnd mozTransitionEnd msTransitionEnd oTransitionEnd transitionend", function() {
          if ($navMenu.hasClass('active')) {
            if ($clickedOpenButton.is(searchOpenSelector)) {
              // if the search button was clicked, wait for CSS animation to finish and then set focus to search field
              $('.kwall-slide-nav-search').focus();
            }
            else {
              // otherwise set focus after timeout to the first link in the nav (which actually is the close nav link
              $navAllLinks.eq(0).focus();
            }
          }
          else {
            $navMenu.addClass('closed');
          }
        }).children().bind("webkitTransitionEnd mozTransitionEnd msTransitionEnd oTransitionEnd transitionend", function(e) {
          // prevent transitionend event from bubbling up
          e.stopPropagation();
          e.preventDefault();
        });

        $searchMenu.bind("webkitTransitionEnd mozTransitionEnd msTransitionEnd oTransitionEnd transitionend", function() {
          if ($searchMenu.hasClass('active')) {
            if ($clickedOpenButton.is(searchOpenSelector)) {
              // if the search button was clicked, wait for CSS animation to finish and then set focus to search field
              $('.kwall-slide-nav-search').focus();
            }
            else {
              // otherwise set focus after timeout to the first link in the nav (which actually is the close nav link
              $navAllLinks.eq(0).focus();
            }
          }
          else {
            $searchMenu.addClass('closed');
          }
        }).children().bind("webkitTransitionEnd mozTransitionEnd msTransitionEnd oTransitionEnd transitionend", function(e) {
          // prevent transitionend event from bubbling up
          e.stopPropagation();
          e.preventDefault();
        });

        // Handle opening the nav
        $navOpenButton.click(function(e) {
          e.preventDefault();
          e.stopPropagation();

          $clickedOpenButton = $(this);

          openNavMenu();
        });

        // Handle opening the search
        $searchOpenButton.click(function(e) {
          e.preventDefault();
          e.stopPropagation();

          $clickedOpenButton = $(this);

          openSearchMenu();
        });

        // Handle closing the slide in with escape key
        $navMenu.attr('tabIndex', '-1').bind('keydown', function(event) {
          if (event.which == ESCAPE_CODE) {
            closeMenu();
          }
        });

        // Handle closing the nav by clicking the close button or the overlay
        $navMenu.find('.slide-menu-close-button').click(function(e) {
          e.preventDefault();
          e.stopPropagation();
          closeMenu();
        });

        $navMenu.siblings('.kwall-slide-menu-overlay').click(function(e) {
          e.preventDefault();
          e.stopPropagation();
          closeMenu();
        });
        $searchMenu.find('.slide-menu-close-button').click(function(e) {
          e.preventDefault();
          e.stopPropagation();
          closeMenu();
        });

        $searchMenu.siblings('.kwall-slide-menu-overlay').click(function(e) {
          e.preventDefault();
          e.stopPropagation();
          closeMenu();
        });
      });
    }
  };

})(jQuery, Drupal, this, this.document);