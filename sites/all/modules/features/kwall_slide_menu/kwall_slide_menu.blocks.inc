<?php

/**
 * @file
 * Block build functions for KWALL Slide Meni
 */

 /**
  * Creates the main nav menu block
  */
function kwall_slide_menu_main_nav_menu($delta) {
  $block = array();

  $module_path = drupal_get_path('module', 'kwall_slide_menu');

  $nav_menu_id = drupal_html_id('kwall-slide-in-nav-menu');

  $block['#attached']['css'] = array($module_path . '/css/kwall_slide_menu.css');
  $block['#attached']['js'] = array(
    $module_path . '/js/kwall_slide_menu.js' => array(
      'type' => 'file',
      'scope' => 'footer',
    ),
    array(
      'data' => array(
        'kwall_slide_menu' => array(
          'searchOpenSelector' => variable_get('kwall_slide_menu_search_open_selector', '.header-search-open-button'),
          'menuOpenSelector' => variable_get('kwall_slide_menu_menu_open_selector', '.header-menu-open-button'),
          'navMenuID' => $nav_menu_id,
        ),
      ),
      'type' => 'setting',
    ),
  );



  $block['main'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('kwall-slide-in-nav-menu', 'closed'),
      'id' => $nav_menu_id,
    ),
  );

  $block['main']['close_nav'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('slide-menu-close'),
    ),
    'close_link' => array(
      '#theme' => 'link',
      '#text' => '<span class="element-invisible">Close Nav</span>',
      '#path' => '',
      '#options' => array(
        'html' => TRUE,
        'fragment' => FALSE,
        'external' => TRUE,
        'attributes' => array(
          'class' => array('slide-menu-close-button'),
          'aria-controls' => $nav_menu_id,
        ),
      ),
    ),
  );
/*
  $block['main']['search'] = drupal_get_form('google_appliance_block_form');
  $block['main']['search']['basic']['keys']['#attributes']['placeholder'] = "Search";
*/
  // Get the menu block render array
  $menu_block = block_load('menu_block', $delta);
  $block['main']['menu'] = _block_get_renderable_array(_block_render_blocks(array($menu_block)));

  $block['overlay'] = array(
    '#theme' => 'link',
      '#text' => '<span class="element-invisible">Close Nav</span>',
      '#path' => '',
      '#options' => array(
        'html' => TRUE,
        'fragment' => FALSE,
        'external' => TRUE,
        'attributes' => array(
          'class' => array('kwall-slide-menu-overlay'),
          'aria-controls' => $nav_menu_id,
        ),
      ),
  );

  return $block;
}
 /**
  * Creates the main nav search menu block
  */
function kwall_slide_menu_main_nav_search_menu($delta) {
  $block = array();

  $module_path = drupal_get_path('module', 'kwall_slide_menu');

  $nav_menu_id = drupal_html_id('kwall-slide-in-nav-search-menu');

  $block['#attached']['css'] = array($module_path . '/css/kwall_slide_menu.css');
  $block['#attached']['js'] = array(
    $module_path . '/js/kwall_slide_menu.js' => array(
      'type' => 'file',
      'scope' => 'footer',
    ),
    array(
      'data' => array(
        'kwall_slide_menu' => array(
          'searchOpenSelector' => variable_get('kwall_slide_menu_search_open_selector', '.header-search-open-button'),
          'menuOpenSelector' => variable_get('kwall_slide_menu_menu_open_selector', '.header-menu-open-button'),
          'navSearchMenuID' => $nav_menu_id,
        ),
      ),
      'type' => 'setting',
    ),
  );



  $block['main'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('kwall-slide-in-nav-search-menu', 'closed'),
      'id' => $nav_menu_id,
    ),
  );

  $block['main']['close_nav'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('slide-menu-close'),
    ),
    'close_link' => array(
      '#theme' => 'link',
      '#text' => '<span class="element-invisible">Close Nav</span>',
      '#path' => '',
      '#options' => array(
        'html' => TRUE,
        'fragment' => FALSE,
        'external' => TRUE,
        'attributes' => array(
          'class' => array('slide-menu-close-button'),
          'aria-controls' => $nav_menu_id,
        ),
      ),
    ),
  );
/*
  $block['main']['search'] = drupal_get_form('google_appliance_block_form');
  $block['main']['search']['basic']['keys']['#attributes']['placeholder'] = "Search";
*/
  // Get the menu block render array
  $menu_block = block_load('menu_block', $delta);
  $block['main']['menu'] = _block_get_renderable_array(_block_render_blocks(array($menu_block)));

  $block['overlay'] = array(
    '#theme' => 'link',
      '#text' => '<span class="element-invisible">Close Nav</span>',
      '#path' => '',
      '#options' => array(
        'html' => TRUE,
        'fragment' => FALSE,
        'external' => TRUE,
        'attributes' => array(
          'class' => array('kwall-slide-menu-overlay'),
          'aria-controls' => $nav_menu_id,
        ),
      ),
  );

  return $block;
}
