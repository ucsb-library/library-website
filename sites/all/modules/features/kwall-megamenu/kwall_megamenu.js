(function ($) {
  Drupal.behaviors.kwall_megamenu = {
    attach: function (context, settings) {
      $('.accessible-mega-menu').once('accessible-mega-menu', function() {
        $('.kwall-megamenu-wrapper', $(this)).accessibleMegaMenu();
      });
    }
  };
})(jQuery);