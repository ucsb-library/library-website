<?php
/**
 * @file
 * Provides theme functions kwall_accordion_menu.
 */

/**
 * Returns HTML for for menu_link__kwall_accordion_menu_nav().
 *
 * Customized version of theme_menu_link
 * Adds a link that can be used for submenu toggling.
 */
function theme_menu_link__kwall_accordion_menu($variables) {
  $element = $variables['element'];
  $sub_menu = '';

  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);

    $output .= '<a href="#" class="submenu-toggle" aria-haspopup="true"><i class="fa fa-plus" aria-hidden="true"></i><span class="element-invisible">Toggle Submenu</span></a><span class="border">&nbsp;</span>';
  }
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Returns HTML for for menu_tree__kwall_accordion_menu_nav().
 *
 * Customized version of theme_menu_tree
 * Adds classes to ul element.
 */
function theme_menu_tree__kwall_accordion_menu($variables) {
  return '<ul class="menu accordion-nav-menu accordion-nav-sub-menu">' . $variables['tree'] . '</ul>';
}

/**
 * Returns HTML for for menu_tree__kwall_accordion_menu_nav__top_level().
 *
 * Customized version of theme_menu_tree
 * Adds classes to ul element for a top level menu.
 */
function theme_menu_tree__kwall_accordion_menu__top_level($variables) {
  return '<ul class="menu accordion-nav-menu accordion-nav-top-level-menu">' . $variables['tree'] . '</ul>';
}
