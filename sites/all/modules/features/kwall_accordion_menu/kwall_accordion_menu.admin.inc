<?php
/**
 * @file
 * Provides admin functions kwall_accordion_menu.
 */

/**
 * Build a settings form.
 */
function kwall_accordion_menu_admin() {
  $form = array();

  $form['kwall_accordion_menu_load_font_awesome'] = array(
    '#type' => 'checkbox',
    '#title' => t('Load Font Awesome Icons'),
    '#description' => t('This module uses Font Awesome Icons. If you already loading Font Awesome in the theme or in another module, uncheck this box.'),
    '#default_value' => variable_get('kwall_accordion_menu_load_font_awesome', 1),
  );

  return system_settings_form($form);
}
