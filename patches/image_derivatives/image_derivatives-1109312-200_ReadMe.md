
  - source of the patch: [https://www.drupal.org/project/drupal/issues/1109312](https://www.drupal.org/project/drupal/issues/1109312)

  -  How to apply the patch:

```
cd $documentRoot/modules/image/
patch < image_derivatives-1109312-200.patch

```
