<!DOCTYPE html>
<html  lang="en">
<head>
<meta charset="utf-8" />
<link rel="shortcut icon" href="https://www.library.ucsb.edu/sites/all/themes/custom/ucsblib_theme/favicon.ico" type="image/vnd.microsoft.icon" />
<title>UCSB Library Libcal Redirect</title>
 <meta name="viewport" content="width=device-width">
<META http-equiv="refresh" content="8; URL=http://libcal.library.ucsb.edu">

<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","//www.google-analytics.com/analytics.js","ga");ga("create", "UA-2234594-1", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");</script>
<!--  -->
  </head>
<body>
</body>
<h1>Redirecting...</h1>
</html>
